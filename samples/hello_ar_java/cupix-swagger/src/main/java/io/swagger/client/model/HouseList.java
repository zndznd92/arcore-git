/**
 * Cupix Einstein API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 2.20.1
 * Contact: inska.lee@cupix.com
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.model;

import io.swagger.client.model.HouseEntity;
import io.swagger.client.model.Pagination;
import java.util.*;
import io.swagger.annotations.*;
import com.google.gson.annotations.SerializedName;

@ApiModel(description = "")
public class HouseList {
  
  @SerializedName("contents")
  private List<HouseEntity> contents = null;
  @SerializedName("pagination")
  private Pagination pagination = null;

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public List<HouseEntity> getContents() {
    return contents;
  }
  public void setContents(List<HouseEntity> contents) {
    this.contents = contents;
  }

  /**
   **/
  @ApiModelProperty(required = true, value = "")
  public Pagination getPagination() {
    return pagination;
  }
  public void setPagination(Pagination pagination) {
    this.pagination = pagination;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    HouseList houseList = (HouseList) o;
    return (this.contents == null ? houseList.contents == null : this.contents.equals(houseList.contents)) &&
        (this.pagination == null ? houseList.pagination == null : this.pagination.equals(houseList.pagination));
  }

  @Override
  public int hashCode() {
    int result = 17;
    result = 31 * result + (this.contents == null ? 0: this.contents.hashCode());
    result = 31 * result + (this.pagination == null ? 0: this.pagination.hashCode());
    return result;
  }

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class HouseList {\n");
    
    sb.append("  contents: ").append(contents).append("\n");
    sb.append("  pagination: ").append(pagination).append("\n");
    sb.append("}\n");
    return sb.toString();
  }
}
