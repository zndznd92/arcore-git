
# Location

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**latitude** | **Float** |  | 
**longitude** | **Float** |  | 
**address** | **String** |  | 



