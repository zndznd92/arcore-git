
# AssetList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Asset&gt;**](Asset.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



