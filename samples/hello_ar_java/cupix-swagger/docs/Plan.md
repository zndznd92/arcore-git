
# Plan

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nextBillingAt** | [**Date**](Date.md) |  |  [optional]
**billingStartedAt** | [**Date**](Date.md) |  |  [optional]
**billingPeriod** | **String** |  |  [optional]
**paidProjectQuantity** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**teamProducts** | [**List&lt;TeamProduct&gt;**](TeamProduct.md) |  |  [optional]



