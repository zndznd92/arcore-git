# UserApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**acceptInvitation**](UserApi.md#acceptInvitation) | **PUT** /users/accept_invitation | Accept team invitation
[**confirmUserEmail**](UserApi.md#confirmUserEmail) | **POST** /users/confirmation/{token} | Confirm user email
[**getInvitation**](UserApi.md#getInvitation) | **GET** /users/invitation | Get invitation info
[**getUsers**](UserApi.md#getUsers) | **GET** /users | Get users
[**getUsersAutocomplete**](UserApi.md#getUsersAutocomplete) | **GET** /users/autocomplete | Get users
[**signup**](UserApi.md#signup) | **POST** /users/signup | User Signup
[**updateUser**](UserApi.md#updateUser) | **PUT** /users/{uid} | Update user


<a name="acceptInvitation"></a>
# **acceptInvitation**
> User acceptInvitation(invitationToken, fields)

Accept team invitation

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
String invitationToken = "invitationToken_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.acceptInvitation(invitationToken, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#acceptInvitation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invitationToken** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="confirmUserEmail"></a>
# **confirmUserEmail**
> User confirmUserEmail(token, fields)

Confirm user email

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
String token = "token_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.confirmUserEmail(token, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#confirmUserEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **token** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getInvitation"></a>
# **getInvitation**
> getInvitation(invitationToken, fields)

Get invitation info

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
String invitationToken = "invitationToken_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.getInvitation(invitationToken, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getInvitation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **invitationToken** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getUsers"></a>
# **getUsers**
> UserList getUsers(projectId, page, perPage, q, filter, orderBy, sort, fields)

Get users

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
Integer projectId = 56; // Integer | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String q = "q_example"; // String | 
String filter = "filter_example"; // String | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    UserList result = apiInstance.getUsers(projectId, page, perPage, q, filter, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **q** | **String**|  | [optional]
 **filter** | **String**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name, role]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**UserList**](UserList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getUsersAutocomplete"></a>
# **getUsersAutocomplete**
> UserAutocompleteResponse getUsersAutocomplete(projectId, q, fields)

Get users

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
Integer projectId = 56; // Integer | 
String q = "q_example"; // String | 
String fields = "fields_example"; // String | 
try {
    UserAutocompleteResponse result = apiInstance.getUsersAutocomplete(projectId, q, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#getUsersAutocomplete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**|  | [optional]
 **q** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**UserAutocompleteResponse**](UserAutocompleteResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="signup"></a>
# **signup**
> User signup(email, firstname, lastname, password, passwordConfirmation, teamName, fields)

User Signup

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
String email = "email_example"; // String | User email
String firstname = "firstname_example"; // String | firstname
String lastname = "lastname_example"; // String | lastname
String password = "password_example"; // String | User password
String passwordConfirmation = "passwordConfirmation_example"; // String | User password confirmation
String teamName = "teamName_example"; // String | team_name
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.signup(email, firstname, lastname, password, passwordConfirmation, teamName, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#signup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| User email |
 **firstname** | **String**| firstname |
 **lastname** | **String**| lastname |
 **password** | **String**| User password |
 **passwordConfirmation** | **String**| User password confirmation |
 **teamName** | **String**| team_name |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateUser"></a>
# **updateUser**
> User updateUser(uid, role, state, fields)

Update user

### Example
```java
// Import classes:
//import io.swagger.client.api.UserApi;

UserApi apiInstance = new UserApi();
Integer uid = 56; // Integer | 
String role = "role_example"; // String | 
String state = "state_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.updateUser(uid, role, state, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling UserApi#updateUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **Integer**|  |
 **role** | **String**|  | [optional]
 **state** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

