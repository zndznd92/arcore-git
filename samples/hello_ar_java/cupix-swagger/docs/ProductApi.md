# ProductApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProducts**](ProductApi.md#getProducts) | **GET** /products | Get Products


<a name="getProducts"></a>
# **getProducts**
> ProductList getProducts(kind, page, perPage, fields)

Get Products

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProductApi;

ProductApi apiInstance = new ProductApi();
String kind = "kind_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    ProductList result = apiInstance.getProducts(kind, page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductApi#getProducts");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kind** | **String**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**ProductList**](ProductList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

