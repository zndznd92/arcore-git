# SessionApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getSession**](SessionApi.md#getSession) | **GET** /sessions | Get current session


<a name="getSession"></a>
# **getSession**
> Session getSession(fields)

Get current session

Retrieves current session&#39;s user and team info

### Example
```java
// Import classes:
//import io.swagger.client.api.SessionApi;

SessionApi apiInstance = new SessionApi();
String fields = "fields_example"; // String | 
try {
    Session result = apiInstance.getSession(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SessionApi#getSession");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Session**](Session.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

