
# ProjectList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Project&gt;**](Project.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



