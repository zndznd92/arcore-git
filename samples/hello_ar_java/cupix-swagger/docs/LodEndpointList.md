
# LodEndpointList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**section** | [**SectionEntity**](SectionEntity.md) |  |  [optional]
**lodEndpoints** | **Object** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



