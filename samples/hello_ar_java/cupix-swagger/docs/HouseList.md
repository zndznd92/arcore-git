
# HouseList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;HouseEntity&gt;**](HouseEntity.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



