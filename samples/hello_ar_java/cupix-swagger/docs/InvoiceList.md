
# InvoiceList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Invoice&gt;**](Invoice.md) |  | 
**hasMore** | **Boolean** |  |  [optional]



