
# Export

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**kind** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**house** | [**HouseEntity**](HouseEntity.md) |  |  [optional]
**meta** | **Object** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



