
# TrashList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Trash&gt;**](Trash.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



