
# Comment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**kind** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**content** | **Object** |  |  [optional]
**house** | **Object** |  |  [optional]
**parent** | **Object** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**deletedAt** | [**Date**](Date.md) |  |  [optional]



