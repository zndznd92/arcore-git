
# JobList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Job&gt;**](Job.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



