
# SectionEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**house** | [**HouseEntity**](HouseEntity.md) |  |  [optional]
**meta** | **Object** |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**lastErrorCode** | **Integer** |  |  [optional]



