# MessageApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createMessage**](MessageApi.md#createMessage) | **POST** /messages | Create Message
[**deleteMessage**](MessageApi.md#deleteMessage) | **DELETE** /messages/{key} | Delete Message
[**getMessage**](MessageApi.md#getMessage) | **GET** /messages/{key} | Message
[**getMessages**](MessageApi.md#getMessages) | **GET** /messages | Messages
[**updateMessage**](MessageApi.md#updateMessage) | **PUT** /messages/{key} | Update Message


<a name="createMessage"></a>
# **createMessage**
> Message createMessage(title, category, description, fields)

Create Message

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.MessageApi;

MessageApi apiInstance = new MessageApi();
String title = "title_example"; // String | 
String category = "category_example"; // String | 
String description = "description_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Message result = apiInstance.createMessage(title, category, description, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MessageApi#createMessage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **title** | **String**|  |
 **category** | **String**|  |
 **description** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteMessage"></a>
# **deleteMessage**
> deleteMessage(key)

Delete Message

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.MessageApi;

MessageApi apiInstance = new MessageApi();
String key = "key_example"; // String | Message ID
try {
    apiInstance.deleteMessage(key);
} catch (ApiException e) {
    System.err.println("Exception when calling MessageApi#deleteMessage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Message ID |

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMessage"></a>
# **getMessage**
> Message getMessage(key, fields)

Message

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.MessageApi;

MessageApi apiInstance = new MessageApi();
String key = "key_example"; // String | Message ID
String fields = "fields_example"; // String | 
try {
    Message result = apiInstance.getMessage(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MessageApi#getMessage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Message ID |
 **fields** | **String**|  | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getMessages"></a>
# **getMessages**
> MessageList getMessages(page, perPage, fields)

Messages

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.MessageApi;

MessageApi apiInstance = new MessageApi();
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    MessageList result = apiInstance.getMessages(page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MessageApi#getMessages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**MessageList**](MessageList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateMessage"></a>
# **updateMessage**
> Message updateMessage(key, title, description, fields)

Update Message

### Example
```java
// Import classes:
//import io.swagger.client.api.MessageApi;

MessageApi apiInstance = new MessageApi();
String key = "key_example"; // String | Message ID
String title = "title_example"; // String | Message Title
String description = "description_example"; // String | Description
String fields = "fields_example"; // String | 
try {
    Message result = apiInstance.updateMessage(key, title, description, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling MessageApi#updateMessage");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Message ID |
 **title** | **String**| Message Title | [optional]
 **description** | **String**| Description | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Message**](Message.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

