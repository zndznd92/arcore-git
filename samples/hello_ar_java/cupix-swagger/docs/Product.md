
# Product

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  | 
**name** | **String** |  |  [optional]
**kind** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**price** | **Float** |  |  [optional]
**meta** | **Object** |  |  [optional]



