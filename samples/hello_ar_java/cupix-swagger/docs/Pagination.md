
# Pagination

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** |  |  [optional]
**totalPages** | **Integer** |  |  [optional]
**perPage** | **Integer** |  |  [optional]
**previousPage** | **Integer** |  |  [optional]
**currentPage** | **Integer** |  |  [optional]
**nextPage** | **Integer** |  |  [optional]
**from** | [**Date**](Date.md) |  |  [optional]
**to** | [**Date**](Date.md) |  |  [optional]
**size** | **Integer** |  |  [optional]



