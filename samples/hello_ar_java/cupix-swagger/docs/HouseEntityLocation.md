
# HouseEntityLocation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**lat** | **Float** |  |  [optional]
**lon** | **Float** |  |  [optional]



