
# OnboardList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | **List&lt;String&gt;** |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



