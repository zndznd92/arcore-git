# FileApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFile**](FileApi.md#createFile) | **POST** /files | Create File
[**deleteFile**](FileApi.md#deleteFile) | **DELETE** /files/{key} | Delete File
[**deleteFileMask**](FileApi.md#deleteFileMask) | **DELETE** /files/{key}/mask | Delete Mask
[**deleteFileMeta**](FileApi.md#deleteFileMeta) | **DELETE** /files/{key}/meta | Delete File Meta
[**downloadFile**](FileApi.md#downloadFile) | **GET** /files/{key}/download | Download file
[**finalizeFileUpload**](FileApi.md#finalizeFileUpload) | **PUT** /files/{key}/finalize_upload | Finalize File Uploading
[**getFile**](FileApi.md#getFile) | **GET** /files/{key} | File
[**getFileDescendants**](FileApi.md#getFileDescendants) | **GET** /files/{key}/descendants | File Descendants
[**getFileMaskUploadUrl**](FileApi.md#getFileMaskUploadUrl) | **GET** /files/{key}/mask_upload_url | Get Mask Upload URL
[**getFileUploadUrl**](FileApi.md#getFileUploadUrl) | **GET** /files/{key}/upload_url | Renew File
[**getFiles**](FileApi.md#getFiles) | **GET** /files | Section Files
[**getForgeEntryUrl**](FileApi.md#getForgeEntryUrl) | **GET** /files/{key}/forge_entry_url | File
[**runFileCommand**](FileApi.md#runFileCommand) | **POST** /files/{key}/run | Run file command
[**updateFile**](FileApi.md#updateFile) | **PUT** /files/{key} | Update File
[**updateFileMeta**](FileApi.md#updateFileMeta) | **PUT** /files/{key}/meta | Update File Meta


<a name="createFile"></a>
# **createFile**
> FileEntity createFile(name, sectionKey, projectId, size, filetype, parentKey, subId, fields)

Create File

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String name = "name_example"; // String | 
String sectionKey = "sectionKey_example"; // String | 
Integer projectId = 56; // Integer | 
Integer size = 56; // Integer | 
String filetype = "filetype_example"; // String | 
String parentKey = "parentKey_example"; // String | 
Integer subId = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.createFile(name, sectionKey, projectId, size, filetype, parentKey, subId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#createFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **sectionKey** | **String**|  | [optional]
 **projectId** | **Integer**|  | [optional]
 **size** | **Integer**|  | [optional]
 **filetype** | **String**|  | [optional]
 **parentKey** | **String**|  | [optional]
 **subId** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteFile"></a>
# **deleteFile**
> deleteFile(key)

Delete File

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
try {
    apiInstance.deleteFile(key);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#deleteFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteFileMask"></a>
# **deleteFileMask**
> deleteFileMask(key, fields)

Delete Mask

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteFileMask(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#deleteFileMask");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteFileMeta"></a>
# **deleteFileMeta**
> FileEntity deleteFileMeta(key, metaKey, fields)

Delete File Meta

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String metaKey = "metaKey_example"; // String | File meta key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.deleteFileMeta(key, metaKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#deleteFileMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **metaKey** | **String**| File meta key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="downloadFile"></a>
# **downloadFile**
> downloadFile(key, fields)

Download file

Redirects file

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    apiInstance.downloadFile(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#downloadFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="finalizeFileUpload"></a>
# **finalizeFileUpload**
> ErrorCode finalizeFileUpload(key, fields)

Finalize File Uploading

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.finalizeFileUpload(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#finalizeFileUpload");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getFile"></a>
# **getFile**
> FileEntity getFile(key, fields)

File

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.getFile(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFileDescendants"></a>
# **getFileDescendants**
> FileList getFileDescendants(key, downloadUrl, page, perPage, orderBy, sort, fields)

File Descendants

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
Boolean downloadUrl = true; // Boolean | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    FileList result = apiInstance.getFileDescendants(key, downloadUrl, page, perPage, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getFileDescendants");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **downloadUrl** | **Boolean**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**FileList**](FileList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFileMaskUploadUrl"></a>
# **getFileMaskUploadUrl**
> FileEntity getFileMaskUploadUrl(key, fields)

Get Mask Upload URL

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.getFileMaskUploadUrl(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getFileMaskUploadUrl");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFileUploadUrl"></a>
# **getFileUploadUrl**
> FileEntity getFileUploadUrl(key, fields)

Renew File

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.getFileUploadUrl(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getFileUploadUrl");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFiles"></a>
# **getFiles**
> FileList getFiles(sectionKey, projectId, downloadUrl, page, perPage, filetype, filter, q, fields)

Section Files

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String sectionKey = "sectionKey_example"; // String | Section key
Integer projectId = 56; // Integer | Project ID
Boolean downloadUrl = true; // Boolean | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String filetype = "filetype_example"; // String | 
String filter = "filter_example"; // String | 
String q = "q_example"; // String | 
String fields = "fields_example"; // String | 
try {
    FileList result = apiInstance.getFiles(sectionKey, projectId, downloadUrl, page, perPage, filetype, filter, q, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getFiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sectionKey** | **String**| Section key | [optional]
 **projectId** | **Integer**| Project ID | [optional]
 **downloadUrl** | **Boolean**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **filetype** | **String**|  | [optional]
 **filter** | **String**|  | [optional]
 **q** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**FileList**](FileList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getForgeEntryUrl"></a>
# **getForgeEntryUrl**
> FileEntity getForgeEntryUrl(key, fields)

File

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.getForgeEntryUrl(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#getForgeEntryUrl");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="runFileCommand"></a>
# **runFileCommand**
> runFileCommand(key, body, fields)

Run file command

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.runFileCommand(key, body, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#runFileCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateFile"></a>
# **updateFile**
> FileEntity updateFile(key, name, thumbnail, fields)

Update File

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String name = "name_example"; // String | File name
File thumbnail = new File("/path/to/file.txt"); // File | File thumbnail
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.updateFile(key, name, thumbnail, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#updateFile");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **name** | **String**| File name | [optional]
 **thumbnail** | **File**| File thumbnail | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateFileMeta"></a>
# **updateFileMeta**
> FileEntity updateFileMeta(key, metaKey, value, fields)

Update File Meta

### Example
```java
// Import classes:
//import io.swagger.client.api.FileApi;

FileApi apiInstance = new FileApi();
String key = "key_example"; // String | File key
String metaKey = "metaKey_example"; // String | Meta key
String value = "value_example"; // String | Meta value
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.updateFileMeta(key, metaKey, value, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling FileApi#updateFileMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| File key |
 **metaKey** | **String**| Meta key |
 **value** | **String**| Meta value | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

