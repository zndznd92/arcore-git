# SearchApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search**](SearchApi.md#search) | **GET** /searches | Integrated search


<a name="search"></a>
# **search**
> SearchResults search(q, kind, houseFilter, projectFilter, page, perPage, orderBy, sort, fields)

Integrated search

### Example
```java
// Import classes:
//import io.swagger.client.api.SearchApi;

SearchApi apiInstance = new SearchApi();
String q = "q_example"; // String | 
String kind = "kind_example"; // String | 
String houseFilter = "houseFilter_example"; // String | 
String projectFilter = "projectFilter_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    SearchResults result = apiInstance.search(q, kind, houseFilter, projectFilter, page, perPage, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SearchApi#search");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**|  | [optional]
 **kind** | **String**|  | [optional]
 **houseFilter** | **String**|  | [optional]
 **projectFilter** | **String**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name, captured_at]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**SearchResults**](SearchResults.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

