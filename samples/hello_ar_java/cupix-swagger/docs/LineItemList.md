
# LineItemList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;LineItem&gt;**](LineItem.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



