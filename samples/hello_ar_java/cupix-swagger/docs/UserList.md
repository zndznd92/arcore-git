
# UserList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;User&gt;**](User.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



