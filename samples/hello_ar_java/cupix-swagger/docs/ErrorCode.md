
# ErrorCode

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **Integer** |  |  [optional]
**reason** | **String** |  |  [optional]



