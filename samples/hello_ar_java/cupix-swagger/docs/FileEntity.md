
# FileEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**bucket** | **String** |  |  [optional]
**size** | **Integer** |  |  [optional]
**filetype** | **String** |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**meta** | **Object** |  |  [optional]
**house** | [**HouseEntity**](HouseEntity.md) |  |  [optional]
**project** | [**Project**](Project.md) |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**action** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
**lodEndpoints** | **Object** |  |  [optional]



