
# StripeLineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **Float** |  |  [optional]
**quantity** | **Float** |  |  [optional]
**proration** | **Boolean** |  |  [optional]
**unitPrice** | **Float** |  |  [optional]
**description** | **String** |  |  [optional]



