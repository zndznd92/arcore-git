# ExportApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getExport**](ExportApi.md#getExport) | **GET** /exports/{id} | Get Export Item
[**getExports**](ExportApi.md#getExports) | **GET** /exports | Get exports
[**updateExport**](ExportApi.md#updateExport) | **PUT** /exports/{id} | Update Export


<a name="getExport"></a>
# **getExport**
> Export getExport(id, fields)

Get Export Item

Get export information

### Example
```java
// Import classes:
//import io.swagger.client.api.ExportApi;

ExportApi apiInstance = new ExportApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    Export result = apiInstance.getExport(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExportApi#getExport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Export**](Export.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getExports"></a>
# **getExports**
> ExportList getExports(houseKey, page, perPage, q, filter, orderBy, sort, fields)

Get exports

### Example
```java
// Import classes:
//import io.swagger.client.api.ExportApi;

ExportApi apiInstance = new ExportApi();
String houseKey = "houseKey_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String q = "q_example"; // String | 
String filter = "filter_example"; // String | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ExportList result = apiInstance.getExports(houseKey, page, perPage, q, filter, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExportApi#getExports");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **q** | **String**|  | [optional]
 **filter** | **String**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**ExportList**](ExportList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateExport"></a>
# **updateExport**
> Export updateExport(id, name, meta, fields)

Update Export

Export update

### Example
```java
// Import classes:
//import io.swagger.client.api.ExportApi;

ExportApi apiInstance = new ExportApi();
Integer id = 56; // Integer | 
String name = "name_example"; // String | 
String meta = "meta_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Export result = apiInstance.updateExport(id, name, meta, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ExportApi#updateExport");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **name** | **String**|  | [optional]
 **meta** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Export**](Export.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

