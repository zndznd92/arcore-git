
# Log

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **String** |  |  [optional]
**house** | [**HouseEntity**](HouseEntity.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**timestamp** | [**Date**](Date.md) |  |  [optional]
**q** | **String** |  |  [optional]



