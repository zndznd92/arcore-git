
# LogList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Log&gt;**](Log.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



