
# SectionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;SectionEntity&gt;**](SectionEntity.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



