# ProjectApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addProjectMember**](ProjectApi.md#addProjectMember) | **POST** /projects/{id}/members | Add Project Member
[**bulkChangeState**](ProjectApi.md#bulkChangeState) | **PUT** /projects/bulk_change_state | Update projects state
[**createProject**](ProjectApi.md#createProject) | **POST** /projects | Create Project
[**deleteProject**](ProjectApi.md#deleteProject) | **DELETE** /projects/{id} | Delete project
[**deleteProjectLoadingLogo**](ProjectApi.md#deleteProjectLoadingLogo) | **DELETE** /projects/{id}/loading_logo | Delete logo
[**deleteProjectLogo**](ProjectApi.md#deleteProjectLogo) | **DELETE** /projects/{id}/logo | Delete logo
[**deleteProjectNadir**](ProjectApi.md#deleteProjectNadir) | **DELETE** /projects/{id}/nadir | Delete project nadir
[**getProject**](ProjectApi.md#getProject) | **GET** /projects/{id} | Get Project
[**getProjectPreference**](ProjectApi.md#getProjectPreference) | **GET** /projects/{id}/preference | Project preference
[**getProjects**](ProjectApi.md#getProjects) | **GET** /projects | Get Project List
[**removeProjectMember**](ProjectApi.md#removeProjectMember) | **DELETE** /projects/{id}/members/{uid} | Remove Member from Project
[**updateProject**](ProjectApi.md#updateProject) | **PUT** /projects/{id} | Update project
[**updateProjectMember**](ProjectApi.md#updateProjectMember) | **PUT** /projects/{id}/members/{uid} | Update project member
[**updateProjectPreference**](ProjectApi.md#updateProjectPreference) | **PUT** /projects/{id}/preference | Update project preference


<a name="addProjectMember"></a>
# **addProjectMember**
> Member addProjectMember(id, email, accessLevel, fields)

Add Project Member

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String email = "email_example"; // String | 
String accessLevel = "accessLevel_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Member result = apiInstance.addProjectMember(id, email, accessLevel, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#addProjectMember");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **email** | **String**|  | [optional]
 **accessLevel** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Member**](Member.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="bulkChangeState"></a>
# **bulkChangeState**
> PurchaseOrder bulkChangeState(projectIds, state, fields)

Update projects state

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
String projectIds = "projectIds_example"; // String | Comma separated project ids to change state 
String state = "state_example"; // String | \"active\", \"inactive\", and \"reject\" are allowed 
String fields = "fields_example"; // String | 
try {
    PurchaseOrder result = apiInstance.bulkChangeState(projectIds, state, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#bulkChangeState");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectIds** | **String**| Comma separated project ids to change state  |
 **state** | **String**| \&quot;active\&quot;, \&quot;inactive\&quot;, and \&quot;reject\&quot; are allowed  | [enum: active, inactive, reject]
 **fields** | **String**|  | [optional]

### Return type

[**PurchaseOrder**](PurchaseOrder.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createProject"></a>
# **createProject**
> Project createProject(name, address, location, size, fields)

Create Project

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
String name = "name_example"; // String | 
String address = "address_example"; // String | 
String location = "location_example"; // String | Serialized location object. latitude, longitude, and name are required as key.
String size = "size_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.createProject(name, address, location, size, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#createProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **name** | **String**|  |
 **address** | **String**|  |
 **location** | **String**| Serialized location object. latitude, longitude, and name are required as key. |
 **size** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteProject"></a>
# **deleteProject**
> deleteProject(id, fields)

Delete project

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteProject(id, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteProjectLoadingLogo"></a>
# **deleteProjectLoadingLogo**
> deleteProjectLoadingLogo(id, fields)

Delete logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteProjectLoadingLogo(id, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProjectLoadingLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteProjectLogo"></a>
# **deleteProjectLogo**
> deleteProjectLogo(id, fields)

Delete logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteProjectLogo(id, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProjectLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteProjectNadir"></a>
# **deleteProjectNadir**
> deleteProjectNadir(id, fields)

Delete project nadir

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteProjectNadir(id, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#deleteProjectNadir");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getProject"></a>
# **getProject**
> Project getProject(id, fields)

Get Project

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.getProject(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getProjectPreference"></a>
# **getProjectPreference**
> Object getProjectPreference(id, fields)

Project preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getProjectPreference(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getProjectPreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getProjects"></a>
# **getProjects**
> ProjectList getProjects(q, filter, orderBy, sort, page, perPage, fields)

Get Project List

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
String q = "q_example"; // String | Project name, address 등을 입력하여 검색할 수 있다. 
String filter = "filter_example"; // String | 조건에 맞는 Project를 필터링 하며, 콤마(,)로 구분된 여러개의 조건을 입력할 수 있다.  가능한 조건 및 입력값 * state={created, active, inactive} 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    ProjectList result = apiInstance.getProjects(q, filter, orderBy, sort, page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#getProjects");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Project name, address 등을 입력하여 검색할 수 있다.  | [optional]
 **filter** | **String**| 조건에 맞는 Project를 필터링 하며, 콤마(,)로 구분된 여러개의 조건을 입력할 수 있다.  가능한 조건 및 입력값 * state&#x3D;{created, active, inactive}  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**ProjectList**](ProjectList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="removeProjectMember"></a>
# **removeProjectMember**
> removeProjectMember(id, uid, fields)

Remove Member from Project

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
Integer uid = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.removeProjectMember(id, uid, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#removeProjectMember");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **uid** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateProject"></a>
# **updateProject**
> Project updateProject(id, name, address, location, state, size, logo, loadingLogo, nadir, fields)

Update project

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String name = "name_example"; // String | 
String address = "address_example"; // String | 
String location = "location_example"; // String | Serialized location object. latitude, longitude, and name are required as key.
String state = "state_example"; // String | 
String size = "size_example"; // String | 
File logo = new File("/path/to/file.txt"); // File | 
File loadingLogo = new File("/path/to/file.txt"); // File | 
File nadir = new File("/path/to/file.txt"); // File | 
String fields = "fields_example"; // String | 
try {
    Project result = apiInstance.updateProject(id, name, address, location, state, size, logo, loadingLogo, nadir, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#updateProject");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **name** | **String**|  | [optional]
 **address** | **String**|  | [optional]
 **location** | **String**| Serialized location object. latitude, longitude, and name are required as key. | [optional]
 **state** | **String**|  | [optional]
 **size** | **String**|  | [optional]
 **logo** | **File**|  | [optional]
 **loadingLogo** | **File**|  | [optional]
 **nadir** | **File**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Project**](Project.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateProjectMember"></a>
# **updateProjectMember**
> Member updateProjectMember(id, uid, accessLevel, fields)

Update project member

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | Project ID
Integer uid = 56; // Integer | 
String accessLevel = "accessLevel_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Member result = apiInstance.updateProjectMember(id, uid, accessLevel, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#updateProjectMember");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**| Project ID |
 **uid** | **Integer**|  |
 **accessLevel** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Member**](Member.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateProjectPreference"></a>
# **updateProjectPreference**
> Object updateProjectPreference(id, body, fields)

Update project preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ProjectApi;

ProjectApi apiInstance = new ProjectApi();
Integer id = 56; // Integer | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateProjectPreference(id, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProjectApi#updateProjectPreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

