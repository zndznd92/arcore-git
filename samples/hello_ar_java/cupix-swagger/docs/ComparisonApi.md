# ComparisonApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createComparison**](ComparisonApi.md#createComparison) | **POST** /comparisons | Create comparison
[**deleteComparison**](ComparisonApi.md#deleteComparison) | **DELETE** /comparisons/{key} | Delete comparison
[**getComparisons**](ComparisonApi.md#getComparisons) | **GET** /comparisons | Get Comparison List
[**updateComparison**](ComparisonApi.md#updateComparison) | **PUT** /comparisons/{key} | Update comparison
[**updateComparisonRank**](ComparisonApi.md#updateComparisonRank) | **PUT** /comparisons/{key}/rank | Update comparison


<a name="createComparison"></a>
# **createComparison**
> Comparison createComparison(sectionKey, targetEntityKey, meta, fields)

Create comparison

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ComparisonApi;

ComparisonApi apiInstance = new ComparisonApi();
String sectionKey = "sectionKey_example"; // String | 
String targetEntityKey = "targetEntityKey_example"; // String | 
String meta = "meta_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comparison result = apiInstance.createComparison(sectionKey, targetEntityKey, meta, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComparisonApi#createComparison");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sectionKey** | **String**|  |
 **targetEntityKey** | **String**|  |
 **meta** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Comparison**](Comparison.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteComparison"></a>
# **deleteComparison**
> deleteComparison(key, fields)

Delete comparison

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ComparisonApi;

ComparisonApi apiInstance = new ComparisonApi();
String key = "key_example"; // String | Comparison key
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteComparison(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling ComparisonApi#deleteComparison");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Comparison key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getComparisons"></a>
# **getComparisons**
> ComparisonList getComparisons(houseKey, page, perPage, orderBy, sort, fields)

Get Comparison List

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ComparisonApi;

ComparisonApi apiInstance = new ComparisonApi();
String houseKey = "houseKey_example"; // String | House key
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    ComparisonList result = apiInstance.getComparisons(houseKey, page, perPage, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComparisonApi#getComparisons");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**| House key |
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name, role, rank]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**ComparisonList**](ComparisonList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateComparison"></a>
# **updateComparison**
> Comparison updateComparison(key, meta, fields)

Update comparison

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ComparisonApi;

ComparisonApi apiInstance = new ComparisonApi();
String key = "key_example"; // String | Comparison key
String meta = "meta_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comparison result = apiInstance.updateComparison(key, meta, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComparisonApi#updateComparison");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Comparison key |
 **meta** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Comparison**](Comparison.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateComparisonRank"></a>
# **updateComparisonRank**
> Comparison updateComparisonRank(key, rankTarget, rankOrder, fields)

Update comparison

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.ComparisonApi;

ComparisonApi apiInstance = new ComparisonApi();
String key = "key_example"; // String | Comparison key
String rankTarget = "rankTarget_example"; // String | Target key
String rankOrder = "rankOrder_example"; // String | before/after
String fields = "fields_example"; // String | 
try {
    Comparison result = apiInstance.updateComparisonRank(key, rankTarget, rankOrder, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ComparisonApi#updateComparisonRank");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Comparison key |
 **rankTarget** | **String**| Target key |
 **rankOrder** | **String**| before/after | [enum: before, after]
 **fields** | **String**|  | [optional]

### Return type

[**Comparison**](Comparison.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

