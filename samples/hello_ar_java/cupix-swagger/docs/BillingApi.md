# BillingApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**beginBillingCycle**](BillingApi.md#beginBillingCycle) | **POST** /billing/begin_billing_cycle | Begin billing cycle
[**createPaymentMethod**](BillingApi.md#createPaymentMethod) | **POST** /billing/payment_method | Create Payment Method
[**createQuote**](BillingApi.md#createQuote) | **POST** /billing/quote | Quote
[**getHistory**](BillingApi.md#getHistory) | **GET** /billing/history | Purchase Order List
[**getInvoice**](BillingApi.md#getInvoice) | **GET** /billing/invoices/{id} | Get Invoice
[**getInvoices**](BillingApi.md#getInvoices) | **GET** /billing/invoices | Invoice List
[**getPackages**](BillingApi.md#getPackages) | **GET** /billing/packages | Package list
[**getPaymentMethod**](BillingApi.md#getPaymentMethod) | **GET** /billing/payment_method | Get Payment Method
[**getPlan**](BillingApi.md#getPlan) | **GET** /billing/plan | Get Plan
[**getPurchaseOrder**](BillingApi.md#getPurchaseOrder) | **GET** /billing/history/:number | Get Purchase Order
[**payInvoice**](BillingApi.md#payInvoice) | **POST** /billing/invoices/{id}/pay | Pay Invoice
[**proceedQuote**](BillingApi.md#proceedQuote) | **PUT** /billing/proceed/{number} | Quote
[**resetBilling**](BillingApi.md#resetBilling) | **DELETE** /billing/plan | Reset billing
[**shiftBillingDate**](BillingApi.md#shiftBillingDate) | **PUT** /billing/shift_billing_date | Shift billing date
[**updateBillingPeriod**](BillingApi.md#updateBillingPeriod) | **PUT** /billing/billing_period | Change billing period
[**updatePaymentMethod**](BillingApi.md#updatePaymentMethod) | **PUT** /billing/payment_method | Update Payment Method


<a name="beginBillingCycle"></a>
# **beginBillingCycle**
> beginBillingCycle(fields)

Begin billing cycle

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.beginBillingCycle(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#beginBillingCycle");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createPaymentMethod"></a>
# **createPaymentMethod**
> PaymentMethod createPaymentMethod(source, fields)

Create Payment Method

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String source = "source_example"; // String | Payment method source
String fields = "fields_example"; // String | 
try {
    PaymentMethod result = apiInstance.createPaymentMethod(source, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#createPaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source** | **String**| Payment method source |
 **fields** | **String**|  | [optional]

### Return type

[**PaymentMethod**](PaymentMethod.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createQuote"></a>
# **createQuote**
> PurchaseOrder createQuote(codes, billingPeriod, fields)

Quote

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String codes = "codes_example"; // String | Comman separated product code
String billingPeriod = "billingPeriod_example"; // String | Billing period when quote a plan
String fields = "fields_example"; // String | 
try {
    PurchaseOrder result = apiInstance.createQuote(codes, billingPeriod, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#createQuote");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **codes** | **String**| Comman separated product code |
 **billingPeriod** | **String**| Billing period when quote a plan | [optional] [enum: null, month, year]
 **fields** | **String**|  | [optional]

### Return type

[**PurchaseOrder**](PurchaseOrder.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getHistory"></a>
# **getHistory**
> PurchaseOrderList getHistory(page, perPage, fields)

Purchase Order List

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    PurchaseOrderList result = apiInstance.getHistory(page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getHistory");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**PurchaseOrderList**](PurchaseOrderList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getInvoice"></a>
# **getInvoice**
> Invoice getInvoice(id, fields)

Get Invoice

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String id = "id_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Invoice result = apiInstance.getInvoice(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getInvoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Invoice**](Invoice.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getInvoices"></a>
# **getInvoices**
> InvoiceList getInvoices(startingAfter, fields)

Invoice List

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String startingAfter = "startingAfter_example"; // String | 
String fields = "fields_example"; // String | 
try {
    InvoiceList result = apiInstance.getInvoices(startingAfter, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getInvoices");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startingAfter** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**InvoiceList**](InvoiceList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPackages"></a>
# **getPackages**
> ProductList getPackages(page, perPage, fields)

Package list

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    ProductList result = apiInstance.getPackages(page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getPackages");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**ProductList**](ProductList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPaymentMethod"></a>
# **getPaymentMethod**
> PaymentMethod getPaymentMethod(fields)

Get Payment Method

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String fields = "fields_example"; // String | 
try {
    PaymentMethod result = apiInstance.getPaymentMethod(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getPaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**PaymentMethod**](PaymentMethod.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPlan"></a>
# **getPlan**
> PlanResponse getPlan(fields)

Get Plan

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String fields = "fields_example"; // String | 
try {
    PlanResponse result = apiInstance.getPlan(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getPlan");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**PlanResponse**](PlanResponse.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPurchaseOrder"></a>
# **getPurchaseOrder**
> PurchaseOrder getPurchaseOrder(number, page, perPage, fields)

Get Purchase Order

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String number = "number_example"; // String | Purchase order number
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    PurchaseOrder result = apiInstance.getPurchaseOrder(number, page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#getPurchaseOrder");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**| Purchase order number |
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**PurchaseOrder**](PurchaseOrder.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="payInvoice"></a>
# **payInvoice**
> Invoice payInvoice(id, fields)

Pay Invoice

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String id = "id_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Invoice result = apiInstance.payInvoice(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#payInvoice");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Invoice**](Invoice.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="proceedQuote"></a>
# **proceedQuote**
> Plan proceedQuote(number, fields)

Quote

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String number = "number_example"; // String | Purchase order number
String fields = "fields_example"; // String | 
try {
    Plan result = apiInstance.proceedQuote(number, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#proceedQuote");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **number** | **String**| Purchase order number |
 **fields** | **String**|  | [optional]

### Return type

[**Plan**](Plan.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="resetBilling"></a>
# **resetBilling**
> resetBilling(fields)

Reset billing

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.resetBilling(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#resetBilling");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="shiftBillingDate"></a>
# **shiftBillingDate**
> shiftBillingDate(days, fields)

Shift billing date

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
BigDecimal days = new BigDecimal(); // BigDecimal | 
String fields = "fields_example"; // String | 
try {
    apiInstance.shiftBillingDate(days, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#shiftBillingDate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **days** | **BigDecimal**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateBillingPeriod"></a>
# **updateBillingPeriod**
> PurchaseOrder updateBillingPeriod(billingPeriod, fields)

Change billing period

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String billingPeriod = "billingPeriod_example"; // String | Billing period
String fields = "fields_example"; // String | 
try {
    PurchaseOrder result = apiInstance.updateBillingPeriod(billingPeriod, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#updateBillingPeriod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billingPeriod** | **String**| Billing period | [enum: null, month, year]
 **fields** | **String**|  | [optional]

### Return type

[**PurchaseOrder**](PurchaseOrder.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updatePaymentMethod"></a>
# **updatePaymentMethod**
> PaymentMethod updatePaymentMethod(source, fields)

Update Payment Method

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.BillingApi;

BillingApi apiInstance = new BillingApi();
String source = "source_example"; // String | Payment method source
String fields = "fields_example"; // String | 
try {
    PaymentMethod result = apiInstance.updatePaymentMethod(source, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling BillingApi#updatePaymentMethod");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **source** | **String**| Payment method source |
 **fields** | **String**|  | [optional]

### Return type

[**PaymentMethod**](PaymentMethod.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

