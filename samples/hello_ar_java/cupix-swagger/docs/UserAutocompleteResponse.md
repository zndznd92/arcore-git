
# UserAutocompleteResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;User&gt;**](User.md) |  | 
**size** | **Integer** |  | 



