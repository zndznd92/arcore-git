
# PlanResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nextBillingAt** | [**Date**](Date.md) |  |  [optional]
**next** | [**Plan**](Plan.md) |  |  [optional]
**current** | [**Plan**](Plan.md) |  |  [optional]



