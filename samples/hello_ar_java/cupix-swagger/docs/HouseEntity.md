
# HouseEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**name** | **String** |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**state** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**share** | [**HouseEntityShare**](HouseEntityShare.md) |  |  [optional]
**address** | **String** |  |  [optional]
**player** | **Object** |  |  [optional]
**project** | [**Project**](Project.md) |  |  [optional]
**team** | [**Team**](Team.md) |  |  [optional]
**acl** | [**Acl**](Acl.md) |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**coverUrl** | **String** |  |  [optional]
**meta** | **Object** |  |  [optional]
**location** | [**HouseEntityLocation**](HouseEntityLocation.md) |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**publishedAt** | [**Date**](Date.md) |  |  [optional]
**deletedAt** | [**Date**](Date.md) |  |  [optional]
**favoritorIds** | **List&lt;Integer&gt;** |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**publicUrl** | **String** |  |  [optional]



