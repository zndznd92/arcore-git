
# LineItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**productCode** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**unitCost** | **Float** |  |  [optional]
**quantity** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**cost** | **Float** |  |  [optional]
**type** | **String** |  |  [optional]



