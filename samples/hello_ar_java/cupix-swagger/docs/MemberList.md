
# MemberList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Member&gt;**](Member.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



