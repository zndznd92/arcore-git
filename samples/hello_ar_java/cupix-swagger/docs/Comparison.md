
# Comparison

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**original** | **Object** |  |  [optional]
**target** | **Object** |  |  [optional]
**meta** | **Object** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**rank** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



