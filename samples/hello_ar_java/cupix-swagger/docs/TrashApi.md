# TrashApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**empty**](TrashApi.md#empty) | **DELETE** /trashes/empty | Empty trash
[**getTrashes**](TrashApi.md#getTrashes) | **GET** /trashes | Get trashed items
[**purge**](TrashApi.md#purge) | **PUT** /trashes/purge | Purge trashed items
[**undelete**](TrashApi.md#undelete) | **PUT** /trashes/undelete | Undelete trashed items


<a name="empty"></a>
# **empty**
> empty(fields)

Empty trash

삭제된 모든 House 및 Project를 영구히 삭제한다. 

### Example
```java
// Import classes:
//import io.swagger.client.api.TrashApi;

TrashApi apiInstance = new TrashApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.empty(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TrashApi#empty");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getTrashes"></a>
# **getTrashes**
> TrashList getTrashes(page, perPage, q, orderBy, sort, kind, fields)

Get trashed items

삭제된 House 및 Project 목록을 제공한다. 

### Example
```java
// Import classes:
//import io.swagger.client.api.TrashApi;

TrashApi apiInstance = new TrashApi();
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String q = "q_example"; // String | name, description, address 등 House 엔티티의 속성으로부터 자연어 검색을 하며, 스페이스바 또는 콤마(,)로 구분된 여러 단어를 입력할 경우 검색 시스템 내부 룰에 따라 연산한다. 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String kind = "kind_example"; // String | 
String fields = "fields_example"; // String | 
try {
    TrashList result = apiInstance.getTrashes(page, perPage, q, orderBy, sort, kind, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TrashApi#getTrashes");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **q** | **String**| name, description, address 등 House 엔티티의 속성으로부터 자연어 검색을 하며, 스페이스바 또는 콤마(,)로 구분된 여러 단어를 입력할 경우 검색 시스템 내부 룰에 따라 연산한다.  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **kind** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**TrashList**](TrashList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="purge"></a>
# **purge**
> purge(keys, fields)

Purge trashed items

삭제된 House와 House에 속한 File, Station, Model, Floorplan 등 모든 자원 또는 Project를 영구히 삭제한다. 

### Example
```java
// Import classes:
//import io.swagger.client.api.TrashApi;

TrashApi apiInstance = new TrashApi();
String keys = "keys_example"; // String | Comma separated keys of trash
String fields = "fields_example"; // String | 
try {
    apiInstance.purge(keys, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TrashApi#purge");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keys** | **String**| Comma separated keys of trash |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="undelete"></a>
# **undelete**
> undelete(keys, fields)

Undelete trashed items

삭제된 House 또는 Project를 복원한다. 

### Example
```java
// Import classes:
//import io.swagger.client.api.TrashApi;

TrashApi apiInstance = new TrashApi();
String keys = "keys_example"; // String | Comma separated keys of trash
String fields = "fields_example"; // String | 
try {
    apiInstance.undelete(keys, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TrashApi#undelete");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **keys** | **String**| Comma separated keys of trash |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

