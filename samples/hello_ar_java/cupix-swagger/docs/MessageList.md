
# MessageList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Message&gt;**](Message.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



