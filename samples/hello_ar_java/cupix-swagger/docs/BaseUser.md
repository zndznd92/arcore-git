
# BaseUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**email** | **String** |  |  [optional]
**userType** | **String** |  |  [optional]
**firstname** | **String** |  |  [optional]
**lastname** | **String** |  |  [optional]



