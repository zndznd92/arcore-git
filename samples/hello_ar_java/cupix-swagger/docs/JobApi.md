# JobApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getJob**](JobApi.md#getJob) | **GET** /jobs/{id} | Get job
[**getJobs**](JobApi.md#getJobs) | **GET** /jobs | Get Jobs
[**stopJob**](JobApi.md#stopJob) | **PUT** /jobs/{id}/stop | Stop
[**updateJob**](JobApi.md#updateJob) | **PUT** /jobs/{id} | Update job


<a name="getJob"></a>
# **getJob**
> Job getJob(id, fields)

Get job

Get job information

### Example
```java
// Import classes:
//import io.swagger.client.api.JobApi;

JobApi apiInstance = new JobApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    Job result = apiInstance.getJob(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#getJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getJobs"></a>
# **getJobs**
> JobList getJobs(houseKey, sectionKey, fileKey, page, perPage, q, filter, orderBy, sort, fields)

Get Jobs

### Example
```java
// Import classes:
//import io.swagger.client.api.JobApi;

JobApi apiInstance = new JobApi();
String houseKey = "houseKey_example"; // String | 
String sectionKey = "sectionKey_example"; // String | 
String fileKey = "fileKey_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String q = "q_example"; // String | 
String filter = "filter_example"; // String | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    JobList result = apiInstance.getJobs(houseKey, sectionKey, fileKey, page, perPage, q, filter, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#getJobs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  | [optional]
 **sectionKey** | **String**|  | [optional]
 **fileKey** | **String**|  | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **q** | **String**|  | [optional]
 **filter** | **String**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**JobList**](JobList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="stopJob"></a>
# **stopJob**
> Job stopJob(id, fields)

Stop

Stops running task

### Example
```java
// Import classes:
//import io.swagger.client.api.JobApi;

JobApi apiInstance = new JobApi();
Integer id = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    Job result = apiInstance.stopJob(id, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#stopJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateJob"></a>
# **updateJob**
> Job updateJob(id, progress, processingStatus, state, errorCode, taskId, fields)

Update job

Job update

### Example
```java
// Import classes:
//import io.swagger.client.api.JobApi;

JobApi apiInstance = new JobApi();
Integer id = 56; // Integer | 
Float progress = 3.4F; // Float | 
String processingStatus = "processingStatus_example"; // String | 
String state = "state_example"; // String | 
Integer errorCode = 56; // Integer | 
String taskId = "taskId_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Job result = apiInstance.updateJob(id, progress, processingStatus, state, errorCode, taskId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling JobApi#updateJob");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Integer**|  |
 **progress** | **Float**|  | [optional]
 **processingStatus** | **String**|  | [optional]
 **state** | **String**|  | [optional] [enum: processing, timeout, error, success]
 **errorCode** | **Integer**|  | [optional]
 **taskId** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

