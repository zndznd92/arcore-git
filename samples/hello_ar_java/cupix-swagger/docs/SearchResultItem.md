
# SearchResultItem

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**key** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**kind** | **String** |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**meta** | **Object** |  |  [optional]
**acl** | **Object** |  |  [optional]
**state** | **String** |  |  [optional]
**project** | [**Project**](Project.md) |  |  [optional]
**team** | [**Team**](Team.md) |  |  [optional]
**members** | **Object** |  |  [optional]
**memberSize** | **Integer** |  |  [optional]
**description** | **String** |  |  [optional]
**location** | [**Location**](Location.md) |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**favoritorIds** | **List&lt;Integer&gt;** |  |  [optional]
**tags** | **List&lt;String&gt;** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**deletedAt** | [**Date**](Date.md) |  |  [optional]
**publishedAt** | [**Date**](Date.md) |  |  [optional]



