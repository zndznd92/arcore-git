
# TagList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;TagListContents&gt;**](TagListContents.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



