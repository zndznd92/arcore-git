# CommentApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createComment**](CommentApi.md#createComment) | **POST** /comments | Create comment
[**deleteComment**](CommentApi.md#deleteComment) | **DELETE** /comments/{key} | Delete comment
[**getComment**](CommentApi.md#getComment) | **GET** /comments/{key} | Get comment
[**getComments**](CommentApi.md#getComments) | **GET** /comments | Get comment list
[**updateComment**](CommentApi.md#updateComment) | **PUT** /comments/{key} | Update comment


<a name="createComment"></a>
# **createComment**
> Comment createComment(houseKey, parentKey, content, kind, fields)

Create comment

### Example
```java
// Import classes:
//import io.swagger.client.api.CommentApi;

CommentApi apiInstance = new CommentApi();
String houseKey = "houseKey_example"; // String | 
String parentKey = "parentKey_example"; // String | 
String content = "content_example"; // String | serialized json string
String kind = "kind_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comment result = apiInstance.createComment(houseKey, parentKey, content, kind, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommentApi#createComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  | [optional]
 **parentKey** | **String**|  | [optional]
 **content** | **String**| serialized json string | [optional]
 **kind** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Comment**](Comment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteComment"></a>
# **deleteComment**
> deleteComment(key, fields)

Delete comment

### Example
```java
// Import classes:
//import io.swagger.client.api.CommentApi;

CommentApi apiInstance = new CommentApi();
String key = "key_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteComment(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling CommentApi#deleteComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getComment"></a>
# **getComment**
> Comment getComment(key, fields)

Get comment

### Example
```java
// Import classes:
//import io.swagger.client.api.CommentApi;

CommentApi apiInstance = new CommentApi();
String key = "key_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comment result = apiInstance.getComment(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommentApi#getComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Comment**](Comment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getComments"></a>
# **getComments**
> CommentList getComments(houseKey, q, filter, page, perPage, fields)

Get comment list

### Example
```java
// Import classes:
//import io.swagger.client.api.CommentApi;

CommentApi apiInstance = new CommentApi();
String houseKey = "houseKey_example"; // String | 
String q = "q_example"; // String | Query string
String filter = "filter_example"; // String | filter
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    CommentList result = apiInstance.getComments(houseKey, q, filter, page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommentApi#getComments");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  | [optional]
 **q** | **String**| Query string | [optional]
 **filter** | **String**| filter | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**CommentList**](CommentList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateComment"></a>
# **updateComment**
> Comment updateComment(key, content, state, fields)

Update comment

### Example
```java
// Import classes:
//import io.swagger.client.api.CommentApi;

CommentApi apiInstance = new CommentApi();
String key = "key_example"; // String | 
String content = "content_example"; // String | serialized json string
String state = "state_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Comment result = apiInstance.updateComment(key, content, state, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CommentApi#updateComment");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  |
 **content** | **String**| serialized json string | [optional]
 **state** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Comment**](Comment.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

