
# StationEntity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  | 
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**meta** | **Object** |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**previewUrl** | **String** |  |  [optional]
**house** | [**HouseEntity**](HouseEntity.md) |  |  [optional]
**section** | [**SectionEntity**](SectionEntity.md) |  |  [optional]
**file** | [**FileEntity**](FileEntity.md) |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**lod** | **Object** |  |  [optional]
**lodEndpoints** | **Object** |  |  [optional]
**lodList** | **Object** |  |  [optional]
**tile** | **Object** |  |  [optional]
**tileEndpoints** | **Object** |  |  [optional]
**tileList** | **Object** |  |  [optional]



