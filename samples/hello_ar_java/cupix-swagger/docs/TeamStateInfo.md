
# TeamStateInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingState** | **String** |  |  [optional]
**diskState** | **String** |  |  [optional]



