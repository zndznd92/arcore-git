
# Asset

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**project** | [**Project**](Project.md) |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**publishedAt** | [**Date**](Date.md) |  |  [optional]
**resource** | [**FileEntity**](FileEntity.md) |  |  [optional]
**section** | [**SectionEntity**](SectionEntity.md) |  |  [optional]



