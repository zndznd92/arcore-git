
# PurchaseOrderList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;PurchaseOrder&gt;**](PurchaseOrder.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



