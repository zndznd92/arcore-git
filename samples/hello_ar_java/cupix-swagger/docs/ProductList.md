
# ProductList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Product&gt;**](Product.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



