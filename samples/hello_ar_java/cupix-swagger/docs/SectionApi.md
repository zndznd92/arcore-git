# SectionApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**assign**](SectionApi.md#assign) | **PUT** /sections/{key}/assign | Assign section
[**createSection**](SectionApi.md#createSection) | **POST** /sections | Create Section
[**deleteSection**](SectionApi.md#deleteSection) | **DELETE** /sections/{key} | Delete Section
[**deleteSectionMeta**](SectionApi.md#deleteSectionMeta) | **DELETE** /sections/{key}/meta | Delete section meta
[**downloadSection**](SectionApi.md#downloadSection) | **GET** /sections/{key}/download | Download section files
[**getFloorplan**](SectionApi.md#getFloorplan) | **GET** /sections/{key}/floorplan | Section Floorplan
[**getModel**](SectionApi.md#getModel) | **GET** /sections/{key}/model | Section Model
[**getSceneData**](SectionApi.md#getSceneData) | **GET** /sections/{key}/scene_data | Scene data
[**getSection**](SectionApi.md#getSection) | **GET** /sections/{key} | Get a Section
[**getSectionTiles**](SectionApi.md#getSectionTiles) | **GET** /sections/{key}/tiles | Section Tile List
[**getSections**](SectionApi.md#getSections) | **GET** /sections | Section List
[**requestDownloadEmail**](SectionApi.md#requestDownloadEmail) | **POST** /sections/{key}/request_download_email | Request an e-mail to download point cloud
[**resetFloorplan**](SectionApi.md#resetFloorplan) | **DELETE** /sections/{key}/floorplan | Reset Floorplan
[**runSection**](SectionApi.md#runSection) | **POST** /sections/{key}/run | Run command
[**unassign**](SectionApi.md#unassign) | **DELETE** /sections/{key}/assign | Unassign section
[**updateFloorplan**](SectionApi.md#updateFloorplan) | **PUT** /sections/{key}/floorplan | Update Floorplan
[**updateModel**](SectionApi.md#updateModel) | **PUT** /sections/{key}/model | Update Model
[**updateSection**](SectionApi.md#updateSection) | **PUT** /sections/{key} | Update Section
[**updateSectionMeta**](SectionApi.md#updateSectionMeta) | **PUT** /sections/{key}/meta | Update section meta


<a name="assign"></a>
# **assign**
> ErrorCode assign(key, as, fields)

Assign section

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String as = "as_example"; // String | Assign as value
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.assign(key, as, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#assign");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **as** | **String**| Assign as value | [optional] [enum: entry]
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createSection"></a>
# **createSection**
> SectionEntity createSection(houseKey, name, originSectionKey, meta, fields)

Create Section

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String houseKey = "houseKey_example"; // String | 
String name = "name_example"; // String | 
String originSectionKey = "originSectionKey_example"; // String | 
String meta = "meta_example"; // String | 
String fields = "fields_example"; // String | 
try {
    SectionEntity result = apiInstance.createSection(houseKey, name, originSectionKey, meta, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#createSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  |
 **name** | **String**|  | [optional]
 **originSectionKey** | **String**|  | [optional]
 **meta** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**SectionEntity**](SectionEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteSection"></a>
# **deleteSection**
> deleteSection(key, fields)

Delete Section

Returns a Project JSON object

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteSection(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#deleteSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteSectionMeta"></a>
# **deleteSectionMeta**
> FileEntity deleteSectionMeta(key, metaKey, fields)

Delete section meta

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String metaKey = "metaKey_example"; // String | Section meta key
String fields = "fields_example"; // String | 
try {
    FileEntity result = apiInstance.deleteSectionMeta(key, metaKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#deleteSectionMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **metaKey** | **String**| Section meta key |
 **fields** | **String**|  | [optional]

### Return type

[**FileEntity**](FileEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="downloadSection"></a>
# **downloadSection**
> downloadSection(key, filetype, ext, scale, fields)

Download section files

Redirects section file

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String filetype = "filetype_example"; // String | filetype
String ext = "ext_example"; // String | file extension
Float scale = 3.4F; // Float | scale factor
String fields = "fields_example"; // String | 
try {
    apiInstance.downloadSection(key, filetype, ext, scale, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#downloadSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **filetype** | **String**| filetype |
 **ext** | **String**| file extension |
 **scale** | **Float**| scale factor | [optional]
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getFloorplan"></a>
# **getFloorplan**
> Object getFloorplan(key, fields)

Section Floorplan

Returns floorplan

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getFloorplan(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getFloorplan");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getModel"></a>
# **getModel**
> Object getModel(key, fields)

Section Model

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getModel(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getModel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSceneData"></a>
# **getSceneData**
> SceneData getSceneData(key, filetype, filename, ext, scale, fields)

Scene data

Returns scene data such as point cloud, mesh, and texture source.

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String filetype = "filetype_example"; // String | filetype
String filename = "filename_example"; // String | filename
String ext = "ext_example"; // String | file extension
Float scale = 3.4F; // Float | scale factor
String fields = "fields_example"; // String | 
try {
    SceneData result = apiInstance.getSceneData(key, filetype, filename, ext, scale, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getSceneData");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **filetype** | **String**| filetype |
 **filename** | **String**| filename |
 **ext** | **String**| file extension |
 **scale** | **Float**| scale factor | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**SceneData**](SceneData.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSection"></a>
# **getSection**
> SectionEntity getSection(key, fields)

Get a Section

Returns a Section JSON object

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String fields = "fields_example"; // String | 
try {
    SectionEntity result = apiInstance.getSection(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **fields** | **String**|  | [optional]

### Return type

[**SectionEntity**](SectionEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSectionTiles"></a>
# **getSectionTiles**
> StationList getSectionTiles(key, tileType, fields)

Section Tile List

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String tileType = "tileType_example"; // String | blurred or non-blurred
String fields = "fields_example"; // String | 
try {
    StationList result = apiInstance.getSectionTiles(key, tileType, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getSectionTiles");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **tileType** | **String**| blurred or non-blurred | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**StationList**](StationList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getSections"></a>
# **getSections**
> SectionList getSections(houseKey, page, perPage, orderBy, sort, fields)

Section List

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String houseKey = "houseKey_example"; // String | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    SectionList result = apiInstance.getSections(houseKey, page, perPage, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#getSections");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **houseKey** | **String**|  |
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**SectionList**](SectionList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="requestDownloadEmail"></a>
# **requestDownloadEmail**
> requestDownloadEmail(key, filetype, fields)

Request an e-mail to download point cloud

Request an e-mail to download point cloud

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String filetype = "filetype_example"; // String | filetype
String fields = "fields_example"; // String | 
try {
    apiInstance.requestDownloadEmail(key, filetype, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#requestDownloadEmail");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **filetype** | **String**| filetype |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="resetFloorplan"></a>
# **resetFloorplan**
> resetFloorplan(key, fields)

Reset Floorplan

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String fields = "fields_example"; // String | 
try {
    apiInstance.resetFloorplan(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#resetFloorplan");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="runSection"></a>
# **runSection**
> Job runSection(key, command, options, fields)

Run command

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String command = "command_example"; // String | Section command
String options = "options_example"; // String | Section command
String fields = "fields_example"; // String | 
try {
    Job result = apiInstance.runSection(key, command, options, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#runSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **command** | **String**| Section command | [optional]
 **options** | **String**| Section command | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="unassign"></a>
# **unassign**
> ErrorCode unassign(key, as, fields)

Unassign section

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String as = "as_example"; // String | Assign as value
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.unassign(key, as, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#unassign");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **as** | **String**| Assign as value | [optional] [enum: entry]
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateFloorplan"></a>
# **updateFloorplan**
> Object updateFloorplan(key, body, draft, fields)

Update Floorplan

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String body = "body_example"; // String | 
Boolean draft = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateFloorplan(key, body, draft, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#updateFloorplan");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **body** | **String**|  | [optional]
 **draft** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateModel"></a>
# **updateModel**
> Object updateModel(key, body, draft, fields)

Update Model

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String body = "body_example"; // String | 
Boolean draft = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateModel(key, body, draft, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#updateModel");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **body** | **String**|  | [optional]
 **draft** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateSection"></a>
# **updateSection**
> SectionEntity updateSection(key, name, thumbnail, fields)

Update Section

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String name = "name_example"; // String | TODO_Description
File thumbnail = new File("/path/to/file.txt"); // File | Section thumbnail
String fields = "fields_example"; // String | 
try {
    SectionEntity result = apiInstance.updateSection(key, name, thumbnail, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#updateSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **name** | **String**| TODO_Description | [optional]
 **thumbnail** | **File**| Section thumbnail | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**SectionEntity**](SectionEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateSectionMeta"></a>
# **updateSectionMeta**
> SectionEntity updateSectionMeta(key, metaKey, value, fields)

Update section meta

### Example
```java
// Import classes:
//import io.swagger.client.api.SectionApi;

SectionApi apiInstance = new SectionApi();
String key = "key_example"; // String | Section key
String metaKey = "metaKey_example"; // String | Meta key
String value = "value_example"; // String | Meta value
String fields = "fields_example"; // String | 
try {
    SectionEntity result = apiInstance.updateSectionMeta(key, metaKey, value, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling SectionApi#updateSectionMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Section key |
 **metaKey** | **String**| Meta key |
 **value** | **String**| Meta value | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**SectionEntity**](SectionEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

