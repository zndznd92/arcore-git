
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**email** | **String** |  |  [optional]
**userType** | **String** |  |  [optional]
**firstname** | **String** |  |  [optional]
**lastname** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**accountType** | **String** |  |  [optional]
**role** | **String** |  |  [optional]
**roles** | **List&lt;String&gt;** |  |  [optional]
**phoneNumber** | **String** |  |  [optional]
**userToken** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]
**logoUrl** | **String** |  |  [optional]
**nadirUrl** | **String** |  |  [optional]
**settings** | **Object** |  |  [optional]
**notificationSettings** | **Object** |  |  [optional]
**accessToken** | **String** |  |  [optional]
**refreshToken** | **String** |  |  [optional]
**expiresAt** | [**Date**](Date.md) |  |  [optional]
**team** | [**Team**](Team.md) |  |  [optional]
**projectAcl** | **Object** | admin &amp; rw - admin / rw - collaborator / ro - viewer  |  [optional]



