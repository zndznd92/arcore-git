
# Invoice

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**amountDue** | **Float** |  |  [optional]
**amountPaid** | **Float** |  |  [optional]
**amountRemaining** | **Float** |  |  [optional]
**closed** | **Boolean** |  |  [optional]
**number** | **String** |  |  [optional]
**receiptNumber** | **String** |  |  [optional]
**subtotal** | **Float** |  |  [optional]
**tax** | **Float** |  |  [optional]
**currency** | **String** |  |  [optional]
**date** | [**Date**](Date.md) |  |  [optional]
**paid** | **Boolean** |  |  [optional]
**total** | **Float** |  |  [optional]
**attempted** | **Boolean** |  |  [optional]
**attemptCount** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**nextPaymentAttempt** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**paidAt** | [**Date**](Date.md) |  |  [optional]
**brand** | **String** |  |  [optional]
**last4** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**lines** | [**List&lt;StripeLineItem&gt;**](StripeLineItem.md) |  |  [optional]



