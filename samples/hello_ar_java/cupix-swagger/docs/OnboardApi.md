# OnboardApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**dismissOnboard**](OnboardApi.md#dismissOnboard) | **PUT** /onboards/{key}/dismiss | Dismiss Onboarding Message
[**getOnboards**](OnboardApi.md#getOnboards) | **GET** /onboards | Onboarding Messages
[**reset**](OnboardApi.md#reset) | **DELETE** /onboards/reset_all | Reset all onboarding message


<a name="dismissOnboard"></a>
# **dismissOnboard**
> ErrorCode dismissOnboard(key, fields)

Dismiss Onboarding Message

### Example
```java
// Import classes:
//import io.swagger.client.api.OnboardApi;

OnboardApi apiInstance = new OnboardApi();
String key = "key_example"; // String | Message key
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.dismissOnboard(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OnboardApi#dismissOnboard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Message key |
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getOnboards"></a>
# **getOnboards**
> OnboardList getOnboards(page, perPage, fields)

Onboarding Messages

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.OnboardApi;

OnboardApi apiInstance = new OnboardApi();
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    OnboardList result = apiInstance.getOnboards(page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OnboardApi#getOnboards");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**OnboardList**](OnboardList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="reset"></a>
# **reset**
> ErrorCode reset(fields)

Reset all onboarding message

### Example
```java
// Import classes:
//import io.swagger.client.api.OnboardApi;

OnboardApi apiInstance = new OnboardApi();
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.reset(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling OnboardApi#reset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

