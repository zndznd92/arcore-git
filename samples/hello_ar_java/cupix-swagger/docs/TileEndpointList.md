
# TileEndpointList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**section** | [**SectionEntity**](SectionEntity.md) |  |  [optional]
**tileEndpoints** | **Object** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



