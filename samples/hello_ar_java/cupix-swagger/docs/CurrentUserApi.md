# CurrentUserApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**deleteAvatar**](CurrentUserApi.md#deleteAvatar) | **DELETE** /me/avatar | Delete My Avatar
[**deleteLoadingLogo**](CurrentUserApi.md#deleteLoadingLogo) | **DELETE** /me/loading_logo | Delete logo
[**deleteLogo**](CurrentUserApi.md#deleteLogo) | **DELETE** /me/logo | Delete logo
[**deleteNadir**](CurrentUserApi.md#deleteNadir) | **DELETE** /me/nadir | Delete nadir
[**getMe**](CurrentUserApi.md#getMe) | **GET** /me | My Info
[**getMyLogs**](CurrentUserApi.md#getMyLogs) | **GET** /me/logs | Get logs
[**getPreference**](CurrentUserApi.md#getPreference) | **GET** /me/preference | My Preference
[**requestConfirmation**](CurrentUserApi.md#requestConfirmation) | **POST** /me/request_confirmation | Request confirmation email
[**requestPhoneNumberVerification**](CurrentUserApi.md#requestPhoneNumberVerification) | **POST** /me/request_phone_number_verification | Phone number verification
[**requestResetPassword**](CurrentUserApi.md#requestResetPassword) | **POST** /me/request_reset_password | Request reset password instruction email
[**resetPassword**](CurrentUserApi.md#resetPassword) | **PUT** /me/reset_password | Reset password with token
[**updateLoadingLogo**](CurrentUserApi.md#updateLoadingLogo) | **PUT** /me/loading_logo | Update my logo
[**updateLogo**](CurrentUserApi.md#updateLogo) | **PUT** /me/logo | Update my logo
[**updateMe**](CurrentUserApi.md#updateMe) | **PUT** /me | Update My Info
[**updateNadir**](CurrentUserApi.md#updateNadir) | **PUT** /me/nadir | Update my nadir
[**updatePreference**](CurrentUserApi.md#updatePreference) | **PUT** /me/preference | Update Preference


<a name="deleteAvatar"></a>
# **deleteAvatar**
> deleteAvatar()

Delete My Avatar

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
try {
    apiInstance.deleteAvatar();
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#deleteAvatar");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteLoadingLogo"></a>
# **deleteLoadingLogo**
> deleteLoadingLogo(fields)

Delete logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteLoadingLogo(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#deleteLoadingLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteLogo"></a>
# **deleteLogo**
> deleteLogo(fields)

Delete logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteLogo(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#deleteLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteNadir"></a>
# **deleteNadir**
> deleteNadir(fields)

Delete nadir

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteNadir(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#deleteNadir");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getMe"></a>
# **getMe**
> User getMe(fields)

My Info

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.getMe(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#getMe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getMyLogs"></a>
# **getMyLogs**
> LogList getMyLogs(kind, period, from, fields)

Get logs

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String kind = "kind_example"; // String | 
Integer period = 56; // Integer | 
Date from = new Date(); // Date | 
String fields = "fields_example"; // String | 
try {
    LogList result = apiInstance.getMyLogs(kind, period, from, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#getMyLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **kind** | **String**|  | [optional]
 **period** | **Integer**|  | [optional]
 **from** | **Date**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**LogList**](LogList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getPreference"></a>
# **getPreference**
> Object getPreference(fields)

My Preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getPreference(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#getPreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="requestConfirmation"></a>
# **requestConfirmation**
> requestConfirmation(email, fields)

Request confirmation email

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String email = "email_example"; // String | email
String fields = "fields_example"; // String | 
try {
    apiInstance.requestConfirmation(email, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#requestConfirmation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="requestPhoneNumberVerification"></a>
# **requestPhoneNumberVerification**
> ErrorCode requestPhoneNumberVerification(phoneNumber, fields)

Phone number verification

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String phoneNumber = "phoneNumber_example"; // String | phone number
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.requestPhoneNumberVerification(phoneNumber, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#requestPhoneNumberVerification");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phoneNumber** | **String**| phone number |
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="requestResetPassword"></a>
# **requestResetPassword**
> User requestResetPassword(email, fields)

Request reset password instruction email

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String email = "email_example"; // String | email
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.requestResetPassword(email, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#requestResetPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="resetPassword"></a>
# **resetPassword**
> User resetPassword(email, resetToken, newPassword, newPasswordConfirm, fields)

Reset password with token

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String email = "email_example"; // String | email
String resetToken = "resetToken_example"; // String | reset password token
String newPassword = "newPassword_example"; // String | new password
String newPasswordConfirm = "newPasswordConfirm_example"; // String | new password confirmation
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.resetPassword(email, resetToken, newPassword, newPasswordConfirm, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#resetPassword");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| email |
 **resetToken** | **String**| reset password token |
 **newPassword** | **String**| new password |
 **newPasswordConfirm** | **String**| new password confirmation |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateLoadingLogo"></a>
# **updateLoadingLogo**
> Object updateLoadingLogo(loadingLogo, fields)

Update my logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
File loadingLogo = new File("/path/to/file.txt"); // File | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateLoadingLogo(loadingLogo, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#updateLoadingLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **loadingLogo** | **File**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateLogo"></a>
# **updateLogo**
> Object updateLogo(logo, fields)

Update my logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
File logo = new File("/path/to/file.txt"); // File | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateLogo(logo, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#updateLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **logo** | **File**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateMe"></a>
# **updateMe**
> User updateMe(firstname, lastname, avatar, password, newPassword, newPasswordConfirm, receiveEmail, receiveText, receiveNewsletter, fields)

Update My Info

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String firstname = "firstname_example"; // String | firstname
String lastname = "lastname_example"; // String | lastname
File avatar = new File("/path/to/file.txt"); // File | User avatar
String password = "password_example"; // String | Existing password
String newPassword = "newPassword_example"; // String | New password
String newPasswordConfirm = "newPasswordConfirm_example"; // String | New password confirmation
Boolean receiveEmail = true; // Boolean | 
Boolean receiveText = true; // Boolean | 
Boolean receiveNewsletter = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.updateMe(firstname, lastname, avatar, password, newPassword, newPasswordConfirm, receiveEmail, receiveText, receiveNewsletter, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#updateMe");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **firstname** | **String**| firstname | [optional]
 **lastname** | **String**| lastname | [optional]
 **avatar** | **File**| User avatar | [optional]
 **password** | **String**| Existing password | [optional]
 **newPassword** | **String**| New password | [optional]
 **newPasswordConfirm** | **String**| New password confirmation | [optional]
 **receiveEmail** | **Boolean**|  | [optional]
 **receiveText** | **Boolean**|  | [optional]
 **receiveNewsletter** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateNadir"></a>
# **updateNadir**
> Object updateNadir(nadir, fields)

Update my nadir

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
File nadir = new File("/path/to/file.txt"); // File | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateNadir(nadir, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#updateNadir");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **nadir** | **File**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updatePreference"></a>
# **updatePreference**
> Object updatePreference(body, fields)

Update Preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.CurrentUserApi;

CurrentUserApi apiInstance = new CurrentUserApi();
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updatePreference(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling CurrentUserApi#updatePreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

