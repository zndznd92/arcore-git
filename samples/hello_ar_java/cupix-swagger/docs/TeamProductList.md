
# TeamProductList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;TeamProduct&gt;**](TeamProduct.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



