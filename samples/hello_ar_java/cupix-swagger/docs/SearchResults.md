
# SearchResults

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;SearchResultItem&gt;**](SearchResultItem.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



