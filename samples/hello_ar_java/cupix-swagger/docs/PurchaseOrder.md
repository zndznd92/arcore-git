
# PurchaseOrder

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**number** | **String** |  | 
**state** | **String** |  |  [optional]
**price** | **Float** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**billingType** | **String** |  |  [optional]
**billingPeriod** | **String** |  |  [optional]
**lineItems** | [**List&lt;LineItem&gt;**](LineItem.md) |  |  [optional]



