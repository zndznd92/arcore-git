
# PaymentMethod

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**brand** | **String** |  |  [optional]
**expMonth** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**expYear** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**last4** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**name** | **String** |  |  [optional]
**addressLine1** | **String** |  |  [optional]
**addressCity** | **String** |  |  [optional]
**addressState** | **String** |  |  [optional]
**addressZip** | **String** |  |  [optional]
**country** | **String** |  |  [optional]



