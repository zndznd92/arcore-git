# AssetApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createAsset**](AssetApi.md#createAsset) | **POST** /assets | Create asset
[**deleteAsset**](AssetApi.md#deleteAsset) | **DELETE** /assets/{key} | Delete asset
[**downloadAsset**](AssetApi.md#downloadAsset) | **GET** /assets/{key}/download | Get asset
[**getAsset**](AssetApi.md#getAsset) | **GET** /assets/{key} | Get asset
[**getAssets**](AssetApi.md#getAssets) | **GET** /assets | Get assets


<a name="createAsset"></a>
# **createAsset**
> Asset createAsset(sectionKey, fileKey, fields)

Create asset

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.AssetApi;

AssetApi apiInstance = new AssetApi();
String sectionKey = "sectionKey_example"; // String | 
String fileKey = "fileKey_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Asset result = apiInstance.createAsset(sectionKey, fileKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetApi#createAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sectionKey** | **String**|  |
 **fileKey** | **String**|  |
 **fields** | **String**|  | [optional]

### Return type

[**Asset**](Asset.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteAsset"></a>
# **deleteAsset**
> deleteAsset(key, fields)

Delete asset

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.AssetApi;

AssetApi apiInstance = new AssetApi();
String key = "key_example"; // String | Asset key
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteAsset(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetApi#deleteAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Asset key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="downloadAsset"></a>
# **downloadAsset**
> downloadAsset(key, fields)

Get asset

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.AssetApi;

AssetApi apiInstance = new AssetApi();
String key = "key_example"; // String | Asset key
String fields = "fields_example"; // String | 
try {
    apiInstance.downloadAsset(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetApi#downloadAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Asset key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAsset"></a>
# **getAsset**
> Asset getAsset(key, fields)

Get asset

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.AssetApi;

AssetApi apiInstance = new AssetApi();
String key = "key_example"; // String | Asset key
String fields = "fields_example"; // String | 
try {
    Asset result = apiInstance.getAsset(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetApi#getAsset");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Asset key |
 **fields** | **String**|  | [optional]

### Return type

[**Asset**](Asset.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getAssets"></a>
# **getAssets**
> AssetList getAssets(sectionKey, fileKey, page, perPage, fields)

Get assets

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.AssetApi;

AssetApi apiInstance = new AssetApi();
String sectionKey = "sectionKey_example"; // String | Section key
String fileKey = "fileKey_example"; // String | Resource key
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    AssetList result = apiInstance.getAssets(sectionKey, fileKey, page, perPage, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AssetApi#getAssets");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sectionKey** | **String**| Section key | [optional]
 **fileKey** | **String**| Resource key | [optional]
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**AssetList**](AssetList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

