# HouseApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**changeOwner**](HouseApi.md#changeOwner) | **PUT** /houses/{key}/chown | Change house owner
[**createHouse**](HouseApi.md#createHouse) | **POST** /houses | Create House
[**deleteCover**](HouseApi.md#deleteCover) | **DELETE** /houses/{key}/cover | Delete Cover
[**deleteHouse**](HouseApi.md#deleteHouse) | **DELETE** /houses/{key} | Deletes House
[**favoriteHouse**](HouseApi.md#favoriteHouse) | **PUT** /houses/{key}/favorite | Mark House as Favorite
[**getAllSharedUsers**](HouseApi.md#getAllSharedUsers) | **GET** /houses/all_shared_users | All Shared Users
[**getGroup**](HouseApi.md#getGroup) | **GET** /houses/{key}/group | House section group
[**getHouse**](HouseApi.md#getHouse) | **GET** /houses/{key} | Get House
[**getHouseLogs**](HouseApi.md#getHouseLogs) | **GET** /houses/{key}/logs | Get house logs
[**getHouseTags**](HouseApi.md#getHouseTags) | **GET** /houses/tags | Get Tags
[**getHouses**](HouseApi.md#getHouses) | **GET** /houses | Get House List
[**moveHouse**](HouseApi.md#moveHouse) | **PUT** /houses/{key}/move | Move house
[**moveSection**](HouseApi.md#moveSection) | **PUT** /houses/{key}/move_section | Move section
[**publishHouse**](HouseApi.md#publishHouse) | **PUT** /houses/{key}/publish | Publish House
[**runHouseCommand**](HouseApi.md#runHouseCommand) | **POST** /houses/{key}/run | Run house command
[**unfavoriteHouse**](HouseApi.md#unfavoriteHouse) | **DELETE** /houses/{key}/favorite | Mark House as Unavorite
[**unpublishHouse**](HouseApi.md#unpublishHouse) | **DELETE** /houses/{key}/publish | Unpublish House
[**updateGroup**](HouseApi.md#updateGroup) | **PUT** /houses/{key}/group | Update group
[**updateHouse**](HouseApi.md#updateHouse) | **PUT** /houses/{key} | Update House


<a name="changeOwner"></a>
# **changeOwner**
> HouseEntity changeOwner(key, email, fields)

Change house owner

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String email = "email_example"; // String | Email
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.changeOwner(key, email, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#changeOwner");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **email** | **String**| Email |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createHouse"></a>
# **createHouse**
> HouseEntity createHouse(projectId, name, address, description, tags, fields)

Create House

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
Integer projectId = 56; // Integer | 
String name = "name_example"; // String | 
String address = "address_example"; // String | 
String description = "description_example"; // String | 
String tags = "tags_example"; // String | 콤마(,)로 구분된 String을 입력해야 하며, 언더바(_)를 제외한 모든 특수문자는 입력해서는 안된다. 
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.createHouse(projectId, name, address, description, tags, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#createHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**|  |
 **name** | **String**|  | [optional]
 **address** | **String**|  | [optional]
 **description** | **String**|  | [optional]
 **tags** | **String**| 콤마(,)로 구분된 String을 입력해야 하며, 언더바(_)를 제외한 모든 특수문자는 입력해서는 안된다.  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteCover"></a>
# **deleteCover**
> deleteCover(key)

Delete Cover

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
try {
    apiInstance.deleteCover(key);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#deleteCover");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteHouse"></a>
# **deleteHouse**
> deleteHouse(key, fields)

Deletes House

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | HOuse key
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteHouse(key, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#deleteHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| HOuse key |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="favoriteHouse"></a>
# **favoriteHouse**
> HouseEntity favoriteHouse(key, fields)

Mark House as Favorite

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.favoriteHouse(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#favoriteHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getAllSharedUsers"></a>
# **getAllSharedUsers**
> UserList getAllSharedUsers(q, fields)

All Shared Users

Shared users from all self owned houses

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String q = "q_example"; // String | Query fields for e-mail auto-completion
String fields = "fields_example"; // String | 
try {
    UserList result = apiInstance.getAllSharedUsers(q, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getAllSharedUsers");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**| Query fields for e-mail auto-completion | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**UserList**](UserList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getGroup"></a>
# **getGroup**
> Object getGroup(key, draft, fields)

House section group

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
Boolean draft = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getGroup(key, draft, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **draft** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getHouse"></a>
# **getHouse**
> HouseEntity getHouse(key, fields)

Get House

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.getHouse(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getHouseLogs"></a>
# **getHouseLogs**
> LogList getHouseLogs(key, kind, period, from, fields)

Get house logs

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String kind = "kind_example"; // String | 
Integer period = 56; // Integer | 
Date from = new Date(); // Date | 
String fields = "fields_example"; // String | 
try {
    LogList result = apiInstance.getHouseLogs(key, kind, period, from, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getHouseLogs");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **kind** | **String**|  | [optional]
 **period** | **Integer**|  | [optional]
 **from** | **Date**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**LogList**](LogList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getHouseTags"></a>
# **getHouseTags**
> TagList getHouseTags(q, scope, fields)

Get Tags

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String q = "q_example"; // String | 
Integer scope = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    TagList result = apiInstance.getHouseTags(q, scope, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getHouseTags");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **q** | **String**|  | [optional]
 **scope** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**TagList**](TagList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getHouses"></a>
# **getHouses**
> HouseList getHouses(projectId, page, perPage, q, tags, filter, orderBy, sort, fields)

Get House List

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
Integer projectId = 56; // Integer | 
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String q = "q_example"; // String | name, description, address 등 House 엔티티의 속성으로부터 자연어 검색을 하며, 스페이스바 또는 콤마(,)로 구분된 여러 단어를 입력할 경우 검색 시스템 내부 룰에 따라 연산한다. 
String tags = "tags_example"; // String | House 엔티티에 적용 된 tag를 필터링하는 옵션으로, 콤마(,)로 구분된 value를 1개 이상 입력 가능하며, AND로 동작한다. 
String filter = "filter_example"; // String | House 엔티티의 속성으로 필터링하는 옵션으로, 미리 정의된 is filter와 name, description 등 House의 속성이 사용 가능하다. 콤마(,)로 구분된 key-value를 1개 이상 입력 가능하며, AND로 동작한다.  사용 가능한 House 속성 * key * name * description * address * lot_size * price * rooms * bathrooms * floors  사용 가능한 is filter * is:publshed : publish 된 House만 선택한다. * is:favorited : 현재 유저가 Favorite 한 House만 선택한다. * is:shared : is_shared_to_me + is_shared_by_me * is:shared_to_me : 현재 유저에게 Share 된 House만 선택한다. * is:shared_by_me : 현재 유저가 Share 한 House만 선택한다. 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    HouseList result = apiInstance.getHouses(projectId, page, perPage, q, tags, filter, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#getHouses");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **projectId** | **Integer**|  |
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **q** | **String**| name, description, address 등 House 엔티티의 속성으로부터 자연어 검색을 하며, 스페이스바 또는 콤마(,)로 구분된 여러 단어를 입력할 경우 검색 시스템 내부 룰에 따라 연산한다.  | [optional]
 **tags** | **String**| House 엔티티에 적용 된 tag를 필터링하는 옵션으로, 콤마(,)로 구분된 value를 1개 이상 입력 가능하며, AND로 동작한다.  | [optional]
 **filter** | **String**| House 엔티티의 속성으로 필터링하는 옵션으로, 미리 정의된 is filter와 name, description 등 House의 속성이 사용 가능하다. 콤마(,)로 구분된 key-value를 1개 이상 입력 가능하며, AND로 동작한다.  사용 가능한 House 속성 * key * name * description * address * lot_size * price * rooms * bathrooms * floors  사용 가능한 is filter * is:publshed : publish 된 House만 선택한다. * is:favorited : 현재 유저가 Favorite 한 House만 선택한다. * is:shared : is_shared_to_me + is_shared_by_me * is:shared_to_me : 현재 유저에게 Share 된 House만 선택한다. * is:shared_by_me : 현재 유저가 Share 한 House만 선택한다.  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name, captured_at]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**HouseList**](HouseList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="moveHouse"></a>
# **moveHouse**
> HouseEntity moveHouse(key, projectId, fields)

Move house

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
Integer projectId = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.moveHouse(key, projectId, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#moveHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **projectId** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="moveSection"></a>
# **moveSection**
> HouseEntity moveSection(key, groupKey, houseKey, fields)

Move section

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String groupKey = "groupKey_example"; // String | group_key
String houseKey = "houseKey_example"; // String | house_key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.moveSection(key, groupKey, houseKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#moveSection");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **groupKey** | **String**| group_key |
 **houseKey** | **String**| house_key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="publishHouse"></a>
# **publishHouse**
> HouseEntity publishHouse(key, fields)

Publish House

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.publishHouse(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#publishHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="runHouseCommand"></a>
# **runHouseCommand**
> Job runHouseCommand(key, body, fields)

Run house command

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | 
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Job result = apiInstance.runHouseCommand(key, body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#runHouseCommand");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**|  |
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Job**](Job.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="unfavoriteHouse"></a>
# **unfavoriteHouse**
> HouseEntity unfavoriteHouse(key, fields)

Mark House as Unavorite

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.unfavoriteHouse(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#unfavoriteHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="unpublishHouse"></a>
# **unpublishHouse**
> HouseEntity unpublishHouse(key, fields)

Unpublish House

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.unpublishHouse(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#unpublishHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateGroup"></a>
# **updateGroup**
> Object updateGroup(key, body, draft, fields)

Update group

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
String body = "body_example"; // String | 
Boolean draft = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateGroup(key, body, draft, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#updateGroup");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **body** | **String**|  | [optional]
 **draft** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="updateHouse"></a>
# **updateHouse**
> HouseEntity updateHouse(key, updatePlayer, name, address, playerOptions, description, highlights, floorList, ogTitle, ogDescription, tags, area, lotSize, price, rooms, bathrooms, floors, thumbnail, cover, encodedCover, coverDraft, encodedCoverDraft, capturedAt, fields)

Update House

### Example
```java
// Import classes:
//import io.swagger.client.api.HouseApi;

HouseApi apiInstance = new HouseApi();
String key = "key_example"; // String | House key
Boolean updatePlayer = true; // Boolean | 
String name = "name_example"; // String | 
String address = "address_example"; // String | 
String playerOptions = "playerOptions_example"; // String | 
String description = "description_example"; // String | 
String highlights = "highlights_example"; // String | 
String floorList = "floorList_example"; // String | 
String ogTitle = "ogTitle_example"; // String | 
String ogDescription = "ogDescription_example"; // String | 
String tags = "tags_example"; // String | 콤마(,)로 구분된 String을 입력해야 하며, 언더바(_)를 제외한 모든 특수문자는 입력해서는 안된다. 
Integer area = 56; // Integer | 
Integer lotSize = 56; // Integer | 
Integer price = 56; // Integer | 
Integer rooms = 56; // Integer | 
Integer bathrooms = 56; // Integer | 
Integer floors = 56; // Integer | 
File thumbnail = new File("/path/to/file.txt"); // File | House thumbnail
File cover = new File("/path/to/file.txt"); // File | House cover
String encodedCover = "encodedCover_example"; // String | Base64 encoded house cover
File coverDraft = new File("/path/to/file.txt"); // File | House cover
String encodedCoverDraft = "encodedCoverDraft_example"; // String | Base64 encoded house cover
String capturedAt = "capturedAt_example"; // String | 
String fields = "fields_example"; // String | 
try {
    HouseEntity result = apiInstance.updateHouse(key, updatePlayer, name, address, playerOptions, description, highlights, floorList, ogTitle, ogDescription, tags, area, lotSize, price, rooms, bathrooms, floors, thumbnail, cover, encodedCover, coverDraft, encodedCoverDraft, capturedAt, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling HouseApi#updateHouse");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| House key |
 **updatePlayer** | **Boolean**|  |
 **name** | **String**|  | [optional]
 **address** | **String**|  | [optional]
 **playerOptions** | **String**|  | [optional]
 **description** | **String**|  | [optional]
 **highlights** | **String**|  | [optional]
 **floorList** | **String**|  | [optional]
 **ogTitle** | **String**|  | [optional]
 **ogDescription** | **String**|  | [optional]
 **tags** | **String**| 콤마(,)로 구분된 String을 입력해야 하며, 언더바(_)를 제외한 모든 특수문자는 입력해서는 안된다.  | [optional]
 **area** | **Integer**|  | [optional]
 **lotSize** | **Integer**|  | [optional]
 **price** | **Integer**|  | [optional]
 **rooms** | **Integer**|  | [optional]
 **bathrooms** | **Integer**|  | [optional]
 **floors** | **Integer**|  | [optional]
 **thumbnail** | **File**| House thumbnail | [optional]
 **cover** | **File**| House cover | [optional]
 **encodedCover** | **String**| Base64 encoded house cover | [optional]
 **coverDraft** | **File**| House cover | [optional]
 **encodedCoverDraft** | **String**| Base64 encoded house cover | [optional]
 **capturedAt** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**HouseEntity**](HouseEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

