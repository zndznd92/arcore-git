
# ExportList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Export&gt;**](Export.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



