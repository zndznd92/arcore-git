
# Team

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**stateInfo** | [**TeamStateInfo**](TeamStateInfo.md) |  |  [optional]
**billingAdmin** | [**User**](User.md) |  |  [optional]
**plan** | [**Plan**](Plan.md) |  |  [optional]
**logoUrl** | **String** |  |  [optional]
**loadingLogoUrl** | **String** |  |  [optional]
**nadirUrl** | **String** |  |  [optional]
**preference** | **Object** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**deletedAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**teamType** | **String** |  |  [optional]
**organization** | [**Organization**](Organization.md) |  |  [optional]
**accountManager** | [**AccountManager**](AccountManager.md) |  |  [optional]



