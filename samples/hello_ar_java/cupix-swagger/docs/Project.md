
# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**name** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**acl** | [**Acl**](Acl.md) |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**location** | [**Location**](Location.md) |  |  [optional]
**team** | [**Team**](Team.md) |  |  [optional]
**meta** | **Object** |  |  [optional]
**logoUrl** | **String** |  |  [optional]
**loadingLogoUrl** | **String** |  |  [optional]
**nadirUrl** | **String** |  |  [optional]
**preference** | **Object** |  |  [optional]
**members** | **Object** |  |  [optional]
**memberSize** | **Integer** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



