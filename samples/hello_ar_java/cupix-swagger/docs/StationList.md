
# StationList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;StationEntity&gt;**](StationEntity.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  |  [optional]



