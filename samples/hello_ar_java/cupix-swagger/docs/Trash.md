
# Trash

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**kind** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**owner** | [**User**](User.md) |  |  [optional]
**thumbnailUrl** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**deletedAt** | [**Date**](Date.md) |  |  [optional]



