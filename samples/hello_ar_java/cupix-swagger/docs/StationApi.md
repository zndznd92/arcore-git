# StationApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**assignStation**](StationApi.md#assignStation) | **PUT** /stations/{key}/assign | Assign station
[**createStation**](StationApi.md#createStation) | **POST** /stations | Create Station
[**deleteStation**](StationApi.md#deleteStation) | **DELETE** /stations/{key} | Delete Station
[**deleteStationMeta**](StationApi.md#deleteStationMeta) | **DELETE** /stations/{key}/meta | Delete Station Meta
[**finalizeStationUpload**](StationApi.md#finalizeStationUpload) | **PUT** /stations/{key}/finalize_upload | Finalize LOD/Tile Upload
[**getLodEndpoints**](StationApi.md#getLodEndpoints) | **GET** /stations/{key}/lod_endpoints | LOD Endpoints
[**getStation**](StationApi.md#getStation) | **GET** /stations/{key} | Get Station
[**getStations**](StationApi.md#getStations) | **GET** /stations | Get Stations
[**getTileEndpoints**](StationApi.md#getTileEndpoints) | **GET** /stations/{key}/tile_endpoints | Tile Endpoints
[**isTileUploaded**](StationApi.md#isTileUploaded) | **GET** /stations/{key}/is_tile_uploaded | Check whether specific object exists with tile level, face, and index
[**unassignStation**](StationApi.md#unassignStation) | **DELETE** /stations/{key}/assign | Unassign station
[**updateStation**](StationApi.md#updateStation) | **PUT** /stations/{key} | Update Station
[**updateStationMeta**](StationApi.md#updateStationMeta) | **PUT** /stations/{key}/meta | Update Station Meta


<a name="assignStation"></a>
# **assignStation**
> ErrorCode assignStation(key, as, fields)

Assign station

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String as = "as_example"; // String | Assign as value
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.assignStation(key, as, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#assignStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **as** | **String**| Assign as value | [optional] [enum: entry]
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createStation"></a>
# **createStation**
> StationEntity createStation(fileKey, sectionKey, lodMaxLevel, maxTileLevel, fields)

Create Station

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String fileKey = "fileKey_example"; // String | 
String sectionKey = "sectionKey_example"; // String | 
Integer lodMaxLevel = 56; // Integer | 
Integer maxTileLevel = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    StationEntity result = apiInstance.createStation(fileKey, sectionKey, lodMaxLevel, maxTileLevel, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#createStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fileKey** | **String**|  |
 **sectionKey** | **String**|  |
 **lodMaxLevel** | **Integer**|  | [optional]
 **maxTileLevel** | **Integer**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**StationEntity**](StationEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteStation"></a>
# **deleteStation**
> deleteStation(key)

Delete Station

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
try {
    apiInstance.deleteStation(key);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#deleteStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="deleteStationMeta"></a>
# **deleteStationMeta**
> StationEntity deleteStationMeta(key, metaKey, fields)

Delete Station Meta

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String metaKey = "metaKey_example"; // String | Station meta key
String fields = "fields_example"; // String | 
try {
    StationEntity result = apiInstance.deleteStationMeta(key, metaKey, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#deleteStationMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **metaKey** | **String**| Station meta key |
 **fields** | **String**|  | [optional]

### Return type

[**StationEntity**](StationEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="finalizeStationUpload"></a>
# **finalizeStationUpload**
> ErrorCode finalizeStationUpload(key, kind, tileType, fields)

Finalize LOD/Tile Upload

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String kind = "kind_example"; // String | Cubemap image type
String tileType = "tileType_example"; // String | blurred or non-blurred
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.finalizeStationUpload(key, kind, tileType, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#finalizeStationUpload");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **kind** | **String**| Cubemap image type | [optional]
 **tileType** | **String**| blurred or non-blurred | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getLodEndpoints"></a>
# **getLodEndpoints**
> LodEndpointList getLodEndpoints(key, fields)

LOD Endpoints

Returns new endpoints that is available to upload lod images.

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String fields = "fields_example"; // String | 
try {
    LodEndpointList result = apiInstance.getLodEndpoints(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#getLodEndpoints");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **fields** | **String**|  | [optional]

### Return type

[**LodEndpointList**](LodEndpointList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getStation"></a>
# **getStation**
> StationEntity getStation(key, fields)

Get Station

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String fields = "fields_example"; // String | 
try {
    StationEntity result = apiInstance.getStation(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#getStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **fields** | **String**|  | [optional]

### Return type

[**StationEntity**](StationEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getStations"></a>
# **getStations**
> StationList getStations(sectionKey, page, perPage, orderBy, sort, fields)

Get Stations

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String sectionKey = "sectionKey_example"; // String | Section key
Integer page = 56; // Integer | 
Integer perPage = 56; // Integer | 
String orderBy = "orderBy_example"; // String | 
String sort = "sort_example"; // String | 
String fields = "fields_example"; // String | 
try {
    StationList result = apiInstance.getStations(sectionKey, page, perPage, orderBy, sort, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#getStations");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sectionKey** | **String**| Section key |
 **page** | **Integer**|  | [optional]
 **perPage** | **Integer**|  | [optional]
 **orderBy** | **String**|  | [optional] [enum: , created_at, updated_at, name]
 **sort** | **String**|  | [optional] [enum: desc, asc]
 **fields** | **String**|  | [optional]

### Return type

[**StationList**](StationList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getTileEndpoints"></a>
# **getTileEndpoints**
> TileEndpointList getTileEndpoints(key, fields)

Tile Endpoints

Returns new endpoints that is available to upload tile images.

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String fields = "fields_example"; // String | 
try {
    TileEndpointList result = apiInstance.getTileEndpoints(key, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#getTileEndpoints");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **fields** | **String**|  | [optional]

### Return type

[**TileEndpointList**](TileEndpointList.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="isTileUploaded"></a>
# **isTileUploaded**
> Object isTileUploaded(key, kind, level, face, index, version, fields)

Check whether specific object exists with tile level, face, and index

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String kind = "kind_example"; // String | Cubemap image type - lod or tile
Integer level = 56; // Integer | 
Integer face = 56; // Integer | 
Integer index = 56; // Integer | 
String version = "version_example"; // String | current or new
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.isTileUploaded(key, kind, level, face, index, version, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#isTileUploaded");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **kind** | **String**| Cubemap image type - lod or tile |
 **level** | **Integer**|  | [optional]
 **face** | **Integer**|  | [optional]
 **index** | **Integer**|  | [optional]
 **version** | **String**| current or new | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="unassignStation"></a>
# **unassignStation**
> ErrorCode unassignStation(key, as, fields)

Unassign station

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String as = "as_example"; // String | Assign as value
String fields = "fields_example"; // String | 
try {
    ErrorCode result = apiInstance.unassignStation(key, as, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#unassignStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **as** | **String**| Assign as value | [optional] [enum: entry]
 **fields** | **String**|  | [optional]

### Return type

[**ErrorCode**](ErrorCode.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateStation"></a>
# **updateStation**
> StationEntity updateStation(key, sectionKey, name, state, lodMaxLevel, maxTileLevel, thumbnail, encodedThumbnail, encodedThumbnailDraft, fields)

Update Station

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String sectionKey = "sectionKey_example"; // String | New section key; section key change allows when new section comes from station's origin section
String name = "name_example"; // String | Station name
String state = "state_example"; // String | Station state
Integer lodMaxLevel = 56; // Integer | 
Integer maxTileLevel = 56; // Integer | 
File thumbnail = new File("/path/to/file.txt"); // File | Station thumbnail
String encodedThumbnail = "encodedThumbnail_example"; // String | Base64 encoded station thumbnail
String encodedThumbnailDraft = "encodedThumbnailDraft_example"; // String | Base64 encoded station thumbnail
String fields = "fields_example"; // String | 
try {
    StationEntity result = apiInstance.updateStation(key, sectionKey, name, state, lodMaxLevel, maxTileLevel, thumbnail, encodedThumbnail, encodedThumbnailDraft, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#updateStation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **sectionKey** | **String**| New section key; section key change allows when new section comes from station&#39;s origin section | [optional]
 **name** | **String**| Station name | [optional]
 **state** | **String**| Station state | [optional]
 **lodMaxLevel** | **Integer**|  | [optional]
 **maxTileLevel** | **Integer**|  | [optional]
 **thumbnail** | **File**| Station thumbnail | [optional]
 **encodedThumbnail** | **String**| Base64 encoded station thumbnail | [optional]
 **encodedThumbnailDraft** | **String**| Base64 encoded station thumbnail | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**StationEntity**](StationEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateStationMeta"></a>
# **updateStationMeta**
> StationEntity updateStationMeta(key, metaKey, value, fields)

Update Station Meta

### Example
```java
// Import classes:
//import io.swagger.client.api.StationApi;

StationApi apiInstance = new StationApi();
String key = "key_example"; // String | Station key
String metaKey = "metaKey_example"; // String | Meta key
String value = "value_example"; // String | Meta value
String fields = "fields_example"; // String | 
try {
    StationEntity result = apiInstance.updateStationMeta(key, metaKey, value, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling StationApi#updateStationMeta");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **key** | **String**| Station key |
 **metaKey** | **String**| Meta key |
 **value** | **String**| Meta value | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**StationEntity**](StationEntity.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

