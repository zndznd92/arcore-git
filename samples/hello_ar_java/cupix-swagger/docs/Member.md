
# Member

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  | 
**email** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**firstname** | **String** |  |  [optional]
**lastname** | **String** |  |  [optional]
**accessLevel** | **String** |  |  [optional]
**memberType** | **String** |  |  [optional]
**avatarUrl** | **String** |  |  [optional]



