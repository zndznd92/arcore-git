
# CommentList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Comment&gt;**](Comment.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



