
# Job

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**state** | **String** |  |  [optional]
**progress** | **Float** |  |  [optional]
**errorCode** | **Integer** |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
**timeout** | **Integer** |  |  [optional]
**instanceType** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]



