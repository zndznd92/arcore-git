
# ComparisonList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;Comparison&gt;**](Comparison.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



