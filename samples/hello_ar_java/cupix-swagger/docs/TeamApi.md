# TeamApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelInvitation**](TeamApi.md#cancelInvitation) | **DELETE** /teams/invitation | Cancel invitation
[**createTeamUser**](TeamApi.md#createTeamUser) | **POST** /teams/users | Create new user
[**deleteTeam**](TeamApi.md#deleteTeam) | **DELETE** /teams | Delete team
[**deleteTeamLoadingLogo**](TeamApi.md#deleteTeamLoadingLogo) | **DELETE** /teams/loading_logo | Delete team loading logo
[**deleteTeamLogo**](TeamApi.md#deleteTeamLogo) | **DELETE** /teams/logo | Delete team logo
[**deleteTeamNadir**](TeamApi.md#deleteTeamNadir) | **DELETE** /teams/nadir | Delete team nadir
[**getTeam**](TeamApi.md#getTeam) | **GET** /teams | Get team
[**getTeamPreference**](TeamApi.md#getTeamPreference) | **GET** /teams/preference | Team preference
[**removeUser**](TeamApi.md#removeUser) | **DELETE** /teams/users/{uid} | Remove user
[**resendInvitation**](TeamApi.md#resendInvitation) | **POST** /teams/resend_invitation | Resend invitation
[**undeleteTeam**](TeamApi.md#undeleteTeam) | **PUT** /teams/undelete | Undelete team
[**updateTeam**](TeamApi.md#updateTeam) | **PUT** /teams | Update team
[**updateTeamPreference**](TeamApi.md#updateTeamPreference) | **PUT** /teams/preference | Update team preference


<a name="cancelInvitation"></a>
# **cancelInvitation**
> cancelInvitation(uid, fields)

Cancel invitation

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
Integer uid = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.cancelInvitation(uid, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#cancelInvitation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="createTeamUser"></a>
# **createTeamUser**
> User createTeamUser(email, firstname, lastname, role, shareCurrentProject, projectAccessLevel, fields)

Create new user

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String email = "email_example"; // String | 
String firstname = "firstname_example"; // String | 
String lastname = "lastname_example"; // String | 
String role = "role_example"; // String | 
Boolean shareCurrentProject = true; // Boolean | 
String projectAccessLevel = "projectAccessLevel_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.createTeamUser(email, firstname, lastname, role, shareCurrentProject, projectAccessLevel, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#createTeamUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**|  |
 **firstname** | **String**|  | [optional]
 **lastname** | **String**|  | [optional]
 **role** | **String**|  | [optional]
 **shareCurrentProject** | **Boolean**|  | [optional]
 **projectAccessLevel** | **String**|  | [optional] [enum: rw, ro]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteTeam"></a>
# **deleteTeam**
> Team deleteTeam(deleteImmediately, fields)

Delete team

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
Boolean deleteImmediately = true; // Boolean | 
String fields = "fields_example"; // String | 
try {
    Team result = apiInstance.deleteTeam(deleteImmediately, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#deleteTeam");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deleteImmediately** | **Boolean**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Team**](Team.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteTeamLoadingLogo"></a>
# **deleteTeamLoadingLogo**
> deleteTeamLoadingLogo(fields)

Delete team loading logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteTeamLoadingLogo(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#deleteTeamLoadingLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteTeamLogo"></a>
# **deleteTeamLogo**
> deleteTeamLogo(fields)

Delete team logo

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteTeamLogo(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#deleteTeamLogo");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="deleteTeamNadir"></a>
# **deleteTeamNadir**
> deleteTeamNadir(fields)

Delete team nadir

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    apiInstance.deleteTeamNadir(fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#deleteTeamNadir");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getTeam"></a>
# **getTeam**
> Team getTeam(fields)

Get team

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    Team result = apiInstance.getTeam(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#getTeam");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Team**](Team.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="getTeamPreference"></a>
# **getTeamPreference**
> Object getTeamPreference(fields)

Team preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.getTeamPreference(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#getTeamPreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="removeUser"></a>
# **removeUser**
> removeUser(uid, forcePurge, takingUserEmail, fields)

Remove user

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
Integer uid = 56; // Integer | 
Boolean forcePurge = true; // Boolean | 
String takingUserEmail = "takingUserEmail_example"; // String | 
String fields = "fields_example"; // String | 
try {
    apiInstance.removeUser(uid, forcePurge, takingUserEmail, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#removeUser");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **Integer**|  |
 **forcePurge** | **Boolean**|  |
 **takingUserEmail** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="resendInvitation"></a>
# **resendInvitation**
> resendInvitation(uid, fields)

Resend invitation

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
Integer uid = 56; // Integer | 
String fields = "fields_example"; // String | 
try {
    apiInstance.resendInvitation(uid, fields);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#resendInvitation");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **uid** | **Integer**|  |
 **fields** | **String**|  | [optional]

### Return type

null (empty response body)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="undeleteTeam"></a>
# **undeleteTeam**
> Team undeleteTeam(fields)

Undelete team

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String fields = "fields_example"; // String | 
try {
    Team result = apiInstance.undeleteTeam(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#undeleteTeam");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Team**](Team.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="updateTeam"></a>
# **updateTeam**
> Team updateTeam(billingAdminEmail, name, logo, loadingLogo, nadir, fields)

Update team

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String billingAdminEmail = "billingAdminEmail_example"; // String | 
String name = "name_example"; // String | 
File logo = new File("/path/to/file.txt"); // File | 
File loadingLogo = new File("/path/to/file.txt"); // File | 
File nadir = new File("/path/to/file.txt"); // File | 
String fields = "fields_example"; // String | 
try {
    Team result = apiInstance.updateTeam(billingAdminEmail, name, logo, loadingLogo, nadir, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#updateTeam");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **billingAdminEmail** | **String**|  | [optional]
 **name** | **String**|  | [optional]
 **logo** | **File**|  | [optional]
 **loadingLogo** | **File**|  | [optional]
 **nadir** | **File**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**Team**](Team.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

<a name="updateTeamPreference"></a>
# **updateTeamPreference**
> Object updateTeamPreference(body, fields)

Update team preference

TODO_Description

### Example
```java
// Import classes:
//import io.swagger.client.api.TeamApi;

TeamApi apiInstance = new TeamApi();
String body = "body_example"; // String | 
String fields = "fields_example"; // String | 
try {
    Object result = apiInstance.updateTeamPreference(body, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling TeamApi#updateTeamPreference");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

**Object**

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

