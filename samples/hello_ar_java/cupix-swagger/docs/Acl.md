
# Acl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rws** | **List&lt;Integer&gt;** |  |  [optional]
**rw** | **List&lt;Integer&gt;** |  |  [optional]
**ro** | **List&lt;Integer&gt;** |  |  [optional]
**owner** | **List&lt;Integer&gt;** |  |  [optional]



