
# FileList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**contents** | [**List&lt;FileEntity&gt;**](FileEntity.md) |  | 
**pagination** | [**Pagination**](Pagination.md) |  | 



