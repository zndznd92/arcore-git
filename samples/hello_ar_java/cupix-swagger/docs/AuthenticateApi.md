# AuthenticateApi

All URIs are relative to *https://webapi.cupix.com/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authenticate**](AuthenticateApi.md#authenticate) | **POST** /authenticate | User Authentication
[**authenticateFacebook**](AuthenticateApi.md#authenticateFacebook) | **POST** /authenticate/facebook | User Authentication with Facebook
[**authenticateFreshdesk**](AuthenticateApi.md#authenticateFreshdesk) | **POST** /authenticate/freshdesk | Sign In to Freshdesk
[**authenticateLinkedin**](AuthenticateApi.md#authenticateLinkedin) | **POST** /authenticate/linkedin | User Authentication with Linkedin
[**refreshToken**](AuthenticateApi.md#refreshToken) | **POST** /authenticate/refresh | Refresh access_token


<a name="authenticate"></a>
# **authenticate**
> User authenticate(email, password, fields)

User Authentication

Authenticates user with email and password

### Example
```java
// Import classes:
//import io.swagger.client.api.AuthenticateApi;

AuthenticateApi apiInstance = new AuthenticateApi();
String email = "email_example"; // String | User email
String password = "password_example"; // String | User password
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.authenticate(email, password, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticateApi#authenticate");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **email** | **String**| User email | [optional]
 **password** | **String**| User password | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="authenticateFacebook"></a>
# **authenticateFacebook**
> User authenticateFacebook(fbAccessToken, state, fields)

User Authentication with Facebook

Authenticates user with Facebook

### Example
```java
// Import classes:
//import io.swagger.client.api.AuthenticateApi;

AuthenticateApi apiInstance = new AuthenticateApi();
String fbAccessToken = "fbAccessToken_example"; // String | Facebook access_token
String state = "state_example"; // String | State
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.authenticateFacebook(fbAccessToken, state, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticateApi#authenticateFacebook");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fbAccessToken** | **String**| Facebook access_token |
 **state** | **String**| State |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="authenticateFreshdesk"></a>
# **authenticateFreshdesk**
> Redirect authenticateFreshdesk(fields)

Sign In to Freshdesk

Authenticates Freshdesk account

### Example
```java
// Import classes:
//import io.swagger.client.api.AuthenticateApi;

AuthenticateApi apiInstance = new AuthenticateApi();
String fields = "fields_example"; // String | 
try {
    Redirect result = apiInstance.authenticateFreshdesk(fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticateApi#authenticateFreshdesk");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **fields** | **String**|  | [optional]

### Return type

[**Redirect**](Redirect.md)

### Authorization

[Bearer](../README.md#Bearer)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="authenticateLinkedin"></a>
# **authenticateLinkedin**
> User authenticateLinkedin(code, state, fields)

User Authentication with Linkedin

Authenticates user with Linkedin

### Example
```java
// Import classes:
//import io.swagger.client.api.AuthenticateApi;

AuthenticateApi apiInstance = new AuthenticateApi();
String code = "code_example"; // String | Linkedin oauth2 code
String state = "state_example"; // String | State
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.authenticateLinkedin(code, state, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticateApi#authenticateLinkedin");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**| Linkedin oauth2 code |
 **state** | **String**| State |
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

<a name="refreshToken"></a>
# **refreshToken**
> User refreshToken(grantType, refreshToken, fields)

Refresh access_token

Authenticates user with email and password

### Example
```java
// Import classes:
//import io.swagger.client.api.AuthenticateApi;

AuthenticateApi apiInstance = new AuthenticateApi();
String grantType = "grantType_example"; // String | Grant type
String refreshToken = "refreshToken_example"; // String | 
String fields = "fields_example"; // String | 
try {
    User result = apiInstance.refreshToken(grantType, refreshToken, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticateApi#refreshToken");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **grantType** | **String**| Grant type | [optional]
 **refreshToken** | **String**|  | [optional]
 **fields** | **String**|  | [optional]

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

