# swagger-android-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-android-client</artifactId>
    <version>1.0.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "io.swagger:swagger-android-client:1.0.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/swagger-android-client-1.0.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import io.swagger.client.api.AssetApi;

public class AssetApiExample {

    public static void main(String[] args) {
        AssetApi apiInstance = new AssetApi();
        String sectionKey = "sectionKey_example"; // String | 
        String fileKey = "fileKey_example"; // String | 
        String fields = "fields_example"; // String | 
        try {
            Asset result = apiInstance.createAsset(sectionKey, fileKey, fields);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AssetApi#createAsset");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://webapi.cupix.com/v2*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AssetApi* | [**createAsset**](docs/AssetApi.md#createAsset) | **POST** /assets | Create asset
*AssetApi* | [**deleteAsset**](docs/AssetApi.md#deleteAsset) | **DELETE** /assets/{key} | Delete asset
*AssetApi* | [**downloadAsset**](docs/AssetApi.md#downloadAsset) | **GET** /assets/{key}/download | Get asset
*AssetApi* | [**getAsset**](docs/AssetApi.md#getAsset) | **GET** /assets/{key} | Get asset
*AssetApi* | [**getAssets**](docs/AssetApi.md#getAssets) | **GET** /assets | Get assets
*AuthenticateApi* | [**authenticate**](docs/AuthenticateApi.md#authenticate) | **POST** /authenticate | User Authentication
*AuthenticateApi* | [**authenticateFacebook**](docs/AuthenticateApi.md#authenticateFacebook) | **POST** /authenticate/facebook | User Authentication with Facebook
*AuthenticateApi* | [**authenticateFreshdesk**](docs/AuthenticateApi.md#authenticateFreshdesk) | **POST** /authenticate/freshdesk | Sign In to Freshdesk
*AuthenticateApi* | [**authenticateLinkedin**](docs/AuthenticateApi.md#authenticateLinkedin) | **POST** /authenticate/linkedin | User Authentication with Linkedin
*AuthenticateApi* | [**refreshToken**](docs/AuthenticateApi.md#refreshToken) | **POST** /authenticate/refresh | Refresh access_token
*BillingApi* | [**beginBillingCycle**](docs/BillingApi.md#beginBillingCycle) | **POST** /billing/begin_billing_cycle | Begin billing cycle
*BillingApi* | [**createPaymentMethod**](docs/BillingApi.md#createPaymentMethod) | **POST** /billing/payment_method | Create Payment Method
*BillingApi* | [**createQuote**](docs/BillingApi.md#createQuote) | **POST** /billing/quote | Quote
*BillingApi* | [**getHistory**](docs/BillingApi.md#getHistory) | **GET** /billing/history | Purchase Order List
*BillingApi* | [**getInvoice**](docs/BillingApi.md#getInvoice) | **GET** /billing/invoices/{id} | Get Invoice
*BillingApi* | [**getInvoices**](docs/BillingApi.md#getInvoices) | **GET** /billing/invoices | Invoice List
*BillingApi* | [**getPackages**](docs/BillingApi.md#getPackages) | **GET** /billing/packages | Package list
*BillingApi* | [**getPaymentMethod**](docs/BillingApi.md#getPaymentMethod) | **GET** /billing/payment_method | Get Payment Method
*BillingApi* | [**getPlan**](docs/BillingApi.md#getPlan) | **GET** /billing/plan | Get Plan
*BillingApi* | [**getPurchaseOrder**](docs/BillingApi.md#getPurchaseOrder) | **GET** /billing/history/:number | Get Purchase Order
*BillingApi* | [**payInvoice**](docs/BillingApi.md#payInvoice) | **POST** /billing/invoices/{id}/pay | Pay Invoice
*BillingApi* | [**proceedQuote**](docs/BillingApi.md#proceedQuote) | **PUT** /billing/proceed/{number} | Quote
*BillingApi* | [**resetBilling**](docs/BillingApi.md#resetBilling) | **DELETE** /billing/plan | Reset billing
*BillingApi* | [**shiftBillingDate**](docs/BillingApi.md#shiftBillingDate) | **PUT** /billing/shift_billing_date | Shift billing date
*BillingApi* | [**updateBillingPeriod**](docs/BillingApi.md#updateBillingPeriod) | **PUT** /billing/billing_period | Change billing period
*BillingApi* | [**updatePaymentMethod**](docs/BillingApi.md#updatePaymentMethod) | **PUT** /billing/payment_method | Update Payment Method
*CommentApi* | [**createComment**](docs/CommentApi.md#createComment) | **POST** /comments | Create comment
*CommentApi* | [**deleteComment**](docs/CommentApi.md#deleteComment) | **DELETE** /comments/{key} | Delete comment
*CommentApi* | [**getComment**](docs/CommentApi.md#getComment) | **GET** /comments/{key} | Get comment
*CommentApi* | [**getComments**](docs/CommentApi.md#getComments) | **GET** /comments | Get comment list
*CommentApi* | [**updateComment**](docs/CommentApi.md#updateComment) | **PUT** /comments/{key} | Update comment
*ComparisonApi* | [**createComparison**](docs/ComparisonApi.md#createComparison) | **POST** /comparisons | Create comparison
*ComparisonApi* | [**deleteComparison**](docs/ComparisonApi.md#deleteComparison) | **DELETE** /comparisons/{key} | Delete comparison
*ComparisonApi* | [**getComparisons**](docs/ComparisonApi.md#getComparisons) | **GET** /comparisons | Get Comparison List
*ComparisonApi* | [**updateComparison**](docs/ComparisonApi.md#updateComparison) | **PUT** /comparisons/{key} | Update comparison
*ComparisonApi* | [**updateComparisonRank**](docs/ComparisonApi.md#updateComparisonRank) | **PUT** /comparisons/{key}/rank | Update comparison
*CurrentUserApi* | [**deleteAvatar**](docs/CurrentUserApi.md#deleteAvatar) | **DELETE** /me/avatar | Delete My Avatar
*CurrentUserApi* | [**deleteLoadingLogo**](docs/CurrentUserApi.md#deleteLoadingLogo) | **DELETE** /me/loading_logo | Delete logo
*CurrentUserApi* | [**deleteLogo**](docs/CurrentUserApi.md#deleteLogo) | **DELETE** /me/logo | Delete logo
*CurrentUserApi* | [**deleteNadir**](docs/CurrentUserApi.md#deleteNadir) | **DELETE** /me/nadir | Delete nadir
*CurrentUserApi* | [**getMe**](docs/CurrentUserApi.md#getMe) | **GET** /me | My Info
*CurrentUserApi* | [**getMyLogs**](docs/CurrentUserApi.md#getMyLogs) | **GET** /me/logs | Get logs
*CurrentUserApi* | [**getPreference**](docs/CurrentUserApi.md#getPreference) | **GET** /me/preference | My Preference
*CurrentUserApi* | [**requestConfirmation**](docs/CurrentUserApi.md#requestConfirmation) | **POST** /me/request_confirmation | Request confirmation email
*CurrentUserApi* | [**requestPhoneNumberVerification**](docs/CurrentUserApi.md#requestPhoneNumberVerification) | **POST** /me/request_phone_number_verification | Phone number verification
*CurrentUserApi* | [**requestResetPassword**](docs/CurrentUserApi.md#requestResetPassword) | **POST** /me/request_reset_password | Request reset password instruction email
*CurrentUserApi* | [**resetPassword**](docs/CurrentUserApi.md#resetPassword) | **PUT** /me/reset_password | Reset password with token
*CurrentUserApi* | [**updateLoadingLogo**](docs/CurrentUserApi.md#updateLoadingLogo) | **PUT** /me/loading_logo | Update my logo
*CurrentUserApi* | [**updateLogo**](docs/CurrentUserApi.md#updateLogo) | **PUT** /me/logo | Update my logo
*CurrentUserApi* | [**updateMe**](docs/CurrentUserApi.md#updateMe) | **PUT** /me | Update My Info
*CurrentUserApi* | [**updateNadir**](docs/CurrentUserApi.md#updateNadir) | **PUT** /me/nadir | Update my nadir
*CurrentUserApi* | [**updatePreference**](docs/CurrentUserApi.md#updatePreference) | **PUT** /me/preference | Update Preference
*ExportApi* | [**getExport**](docs/ExportApi.md#getExport) | **GET** /exports/{id} | Get Export Item
*ExportApi* | [**getExports**](docs/ExportApi.md#getExports) | **GET** /exports | Get exports
*ExportApi* | [**updateExport**](docs/ExportApi.md#updateExport) | **PUT** /exports/{id} | Update Export
*FileApi* | [**createFile**](docs/FileApi.md#createFile) | **POST** /files | Create File
*FileApi* | [**deleteFile**](docs/FileApi.md#deleteFile) | **DELETE** /files/{key} | Delete File
*FileApi* | [**deleteFileMask**](docs/FileApi.md#deleteFileMask) | **DELETE** /files/{key}/mask | Delete Mask
*FileApi* | [**deleteFileMeta**](docs/FileApi.md#deleteFileMeta) | **DELETE** /files/{key}/meta | Delete File Meta
*FileApi* | [**downloadFile**](docs/FileApi.md#downloadFile) | **GET** /files/{key}/download | Download file
*FileApi* | [**finalizeFileUpload**](docs/FileApi.md#finalizeFileUpload) | **PUT** /files/{key}/finalize_upload | Finalize File Uploading
*FileApi* | [**getFile**](docs/FileApi.md#getFile) | **GET** /files/{key} | File
*FileApi* | [**getFileDescendants**](docs/FileApi.md#getFileDescendants) | **GET** /files/{key}/descendants | File Descendants
*FileApi* | [**getFileMaskUploadUrl**](docs/FileApi.md#getFileMaskUploadUrl) | **GET** /files/{key}/mask_upload_url | Get Mask Upload URL
*FileApi* | [**getFileUploadUrl**](docs/FileApi.md#getFileUploadUrl) | **GET** /files/{key}/upload_url | Renew File
*FileApi* | [**getFiles**](docs/FileApi.md#getFiles) | **GET** /files | Section Files
*FileApi* | [**getForgeEntryUrl**](docs/FileApi.md#getForgeEntryUrl) | **GET** /files/{key}/forge_entry_url | File
*FileApi* | [**runFileCommand**](docs/FileApi.md#runFileCommand) | **POST** /files/{key}/run | Run file command
*FileApi* | [**updateFile**](docs/FileApi.md#updateFile) | **PUT** /files/{key} | Update File
*FileApi* | [**updateFileMeta**](docs/FileApi.md#updateFileMeta) | **PUT** /files/{key}/meta | Update File Meta
*HouseApi* | [**changeOwner**](docs/HouseApi.md#changeOwner) | **PUT** /houses/{key}/chown | Change house owner
*HouseApi* | [**createHouse**](docs/HouseApi.md#createHouse) | **POST** /houses | Create House
*HouseApi* | [**deleteCover**](docs/HouseApi.md#deleteCover) | **DELETE** /houses/{key}/cover | Delete Cover
*HouseApi* | [**deleteHouse**](docs/HouseApi.md#deleteHouse) | **DELETE** /houses/{key} | Deletes House
*HouseApi* | [**favoriteHouse**](docs/HouseApi.md#favoriteHouse) | **PUT** /houses/{key}/favorite | Mark House as Favorite
*HouseApi* | [**getAllSharedUsers**](docs/HouseApi.md#getAllSharedUsers) | **GET** /houses/all_shared_users | All Shared Users
*HouseApi* | [**getGroup**](docs/HouseApi.md#getGroup) | **GET** /houses/{key}/group | House section group
*HouseApi* | [**getHouse**](docs/HouseApi.md#getHouse) | **GET** /houses/{key} | Get House
*HouseApi* | [**getHouseLogs**](docs/HouseApi.md#getHouseLogs) | **GET** /houses/{key}/logs | Get house logs
*HouseApi* | [**getHouseTags**](docs/HouseApi.md#getHouseTags) | **GET** /houses/tags | Get Tags
*HouseApi* | [**getHouses**](docs/HouseApi.md#getHouses) | **GET** /houses | Get House List
*HouseApi* | [**moveHouse**](docs/HouseApi.md#moveHouse) | **PUT** /houses/{key}/move | Move house
*HouseApi* | [**moveSection**](docs/HouseApi.md#moveSection) | **PUT** /houses/{key}/move_section | Move section
*HouseApi* | [**publishHouse**](docs/HouseApi.md#publishHouse) | **PUT** /houses/{key}/publish | Publish House
*HouseApi* | [**runHouseCommand**](docs/HouseApi.md#runHouseCommand) | **POST** /houses/{key}/run | Run house command
*HouseApi* | [**unfavoriteHouse**](docs/HouseApi.md#unfavoriteHouse) | **DELETE** /houses/{key}/favorite | Mark House as Unavorite
*HouseApi* | [**unpublishHouse**](docs/HouseApi.md#unpublishHouse) | **DELETE** /houses/{key}/publish | Unpublish House
*HouseApi* | [**updateGroup**](docs/HouseApi.md#updateGroup) | **PUT** /houses/{key}/group | Update group
*HouseApi* | [**updateHouse**](docs/HouseApi.md#updateHouse) | **PUT** /houses/{key} | Update House
*JobApi* | [**getJob**](docs/JobApi.md#getJob) | **GET** /jobs/{id} | Get job
*JobApi* | [**getJobs**](docs/JobApi.md#getJobs) | **GET** /jobs | Get Jobs
*JobApi* | [**stopJob**](docs/JobApi.md#stopJob) | **PUT** /jobs/{id}/stop | Stop
*JobApi* | [**updateJob**](docs/JobApi.md#updateJob) | **PUT** /jobs/{id} | Update job
*MessageApi* | [**createMessage**](docs/MessageApi.md#createMessage) | **POST** /messages | Create Message
*MessageApi* | [**deleteMessage**](docs/MessageApi.md#deleteMessage) | **DELETE** /messages/{key} | Delete Message
*MessageApi* | [**getMessage**](docs/MessageApi.md#getMessage) | **GET** /messages/{key} | Message
*MessageApi* | [**getMessages**](docs/MessageApi.md#getMessages) | **GET** /messages | Messages
*MessageApi* | [**updateMessage**](docs/MessageApi.md#updateMessage) | **PUT** /messages/{key} | Update Message
*OnboardApi* | [**dismissOnboard**](docs/OnboardApi.md#dismissOnboard) | **PUT** /onboards/{key}/dismiss | Dismiss Onboarding Message
*OnboardApi* | [**getOnboards**](docs/OnboardApi.md#getOnboards) | **GET** /onboards | Onboarding Messages
*OnboardApi* | [**reset**](docs/OnboardApi.md#reset) | **DELETE** /onboards/reset_all | Reset all onboarding message
*ProductApi* | [**getProducts**](docs/ProductApi.md#getProducts) | **GET** /products | Get Products
*ProjectApi* | [**addProjectMember**](docs/ProjectApi.md#addProjectMember) | **POST** /projects/{id}/members | Add Project Member
*ProjectApi* | [**bulkChangeState**](docs/ProjectApi.md#bulkChangeState) | **PUT** /projects/bulk_change_state | Update projects state
*ProjectApi* | [**createProject**](docs/ProjectApi.md#createProject) | **POST** /projects | Create Project
*ProjectApi* | [**deleteProject**](docs/ProjectApi.md#deleteProject) | **DELETE** /projects/{id} | Delete project
*ProjectApi* | [**deleteProjectLoadingLogo**](docs/ProjectApi.md#deleteProjectLoadingLogo) | **DELETE** /projects/{id}/loading_logo | Delete logo
*ProjectApi* | [**deleteProjectLogo**](docs/ProjectApi.md#deleteProjectLogo) | **DELETE** /projects/{id}/logo | Delete logo
*ProjectApi* | [**deleteProjectNadir**](docs/ProjectApi.md#deleteProjectNadir) | **DELETE** /projects/{id}/nadir | Delete project nadir
*ProjectApi* | [**getProject**](docs/ProjectApi.md#getProject) | **GET** /projects/{id} | Get Project
*ProjectApi* | [**getProjectPreference**](docs/ProjectApi.md#getProjectPreference) | **GET** /projects/{id}/preference | Project preference
*ProjectApi* | [**getProjects**](docs/ProjectApi.md#getProjects) | **GET** /projects | Get Project List
*ProjectApi* | [**removeProjectMember**](docs/ProjectApi.md#removeProjectMember) | **DELETE** /projects/{id}/members/{uid} | Remove Member from Project
*ProjectApi* | [**updateProject**](docs/ProjectApi.md#updateProject) | **PUT** /projects/{id} | Update project
*ProjectApi* | [**updateProjectMember**](docs/ProjectApi.md#updateProjectMember) | **PUT** /projects/{id}/members/{uid} | Update project member
*ProjectApi* | [**updateProjectPreference**](docs/ProjectApi.md#updateProjectPreference) | **PUT** /projects/{id}/preference | Update project preference
*SearchApi* | [**search**](docs/SearchApi.md#search) | **GET** /searches | Integrated search
*SectionApi* | [**assign**](docs/SectionApi.md#assign) | **PUT** /sections/{key}/assign | Assign section
*SectionApi* | [**createSection**](docs/SectionApi.md#createSection) | **POST** /sections | Create Section
*SectionApi* | [**deleteSection**](docs/SectionApi.md#deleteSection) | **DELETE** /sections/{key} | Delete Section
*SectionApi* | [**deleteSectionMeta**](docs/SectionApi.md#deleteSectionMeta) | **DELETE** /sections/{key}/meta | Delete section meta
*SectionApi* | [**downloadSection**](docs/SectionApi.md#downloadSection) | **GET** /sections/{key}/download | Download section files
*SectionApi* | [**getFloorplan**](docs/SectionApi.md#getFloorplan) | **GET** /sections/{key}/floorplan | Section Floorplan
*SectionApi* | [**getModel**](docs/SectionApi.md#getModel) | **GET** /sections/{key}/model | Section Model
*SectionApi* | [**getSceneData**](docs/SectionApi.md#getSceneData) | **GET** /sections/{key}/scene_data | Scene data
*SectionApi* | [**getSection**](docs/SectionApi.md#getSection) | **GET** /sections/{key} | Get a Section
*SectionApi* | [**getSectionTiles**](docs/SectionApi.md#getSectionTiles) | **GET** /sections/{key}/tiles | Section Tile List
*SectionApi* | [**getSections**](docs/SectionApi.md#getSections) | **GET** /sections | Section List
*SectionApi* | [**requestDownloadEmail**](docs/SectionApi.md#requestDownloadEmail) | **POST** /sections/{key}/request_download_email | Request an e-mail to download point cloud
*SectionApi* | [**resetFloorplan**](docs/SectionApi.md#resetFloorplan) | **DELETE** /sections/{key}/floorplan | Reset Floorplan
*SectionApi* | [**runSection**](docs/SectionApi.md#runSection) | **POST** /sections/{key}/run | Run command
*SectionApi* | [**unassign**](docs/SectionApi.md#unassign) | **DELETE** /sections/{key}/assign | Unassign section
*SectionApi* | [**updateFloorplan**](docs/SectionApi.md#updateFloorplan) | **PUT** /sections/{key}/floorplan | Update Floorplan
*SectionApi* | [**updateModel**](docs/SectionApi.md#updateModel) | **PUT** /sections/{key}/model | Update Model
*SectionApi* | [**updateSection**](docs/SectionApi.md#updateSection) | **PUT** /sections/{key} | Update Section
*SectionApi* | [**updateSectionMeta**](docs/SectionApi.md#updateSectionMeta) | **PUT** /sections/{key}/meta | Update section meta
*SessionApi* | [**getSession**](docs/SessionApi.md#getSession) | **GET** /sessions | Get current session
*StationApi* | [**assignStation**](docs/StationApi.md#assignStation) | **PUT** /stations/{key}/assign | Assign station
*StationApi* | [**createStation**](docs/StationApi.md#createStation) | **POST** /stations | Create Station
*StationApi* | [**deleteStation**](docs/StationApi.md#deleteStation) | **DELETE** /stations/{key} | Delete Station
*StationApi* | [**deleteStationMeta**](docs/StationApi.md#deleteStationMeta) | **DELETE** /stations/{key}/meta | Delete Station Meta
*StationApi* | [**finalizeStationUpload**](docs/StationApi.md#finalizeStationUpload) | **PUT** /stations/{key}/finalize_upload | Finalize LOD/Tile Upload
*StationApi* | [**getLodEndpoints**](docs/StationApi.md#getLodEndpoints) | **GET** /stations/{key}/lod_endpoints | LOD Endpoints
*StationApi* | [**getStation**](docs/StationApi.md#getStation) | **GET** /stations/{key} | Get Station
*StationApi* | [**getStations**](docs/StationApi.md#getStations) | **GET** /stations | Get Stations
*StationApi* | [**getTileEndpoints**](docs/StationApi.md#getTileEndpoints) | **GET** /stations/{key}/tile_endpoints | Tile Endpoints
*StationApi* | [**isTileUploaded**](docs/StationApi.md#isTileUploaded) | **GET** /stations/{key}/is_tile_uploaded | Check whether specific object exists with tile level, face, and index
*StationApi* | [**unassignStation**](docs/StationApi.md#unassignStation) | **DELETE** /stations/{key}/assign | Unassign station
*StationApi* | [**updateStation**](docs/StationApi.md#updateStation) | **PUT** /stations/{key} | Update Station
*StationApi* | [**updateStationMeta**](docs/StationApi.md#updateStationMeta) | **PUT** /stations/{key}/meta | Update Station Meta
*TeamApi* | [**cancelInvitation**](docs/TeamApi.md#cancelInvitation) | **DELETE** /teams/invitation | Cancel invitation
*TeamApi* | [**createTeamUser**](docs/TeamApi.md#createTeamUser) | **POST** /teams/users | Create new user
*TeamApi* | [**deleteTeam**](docs/TeamApi.md#deleteTeam) | **DELETE** /teams | Delete team
*TeamApi* | [**deleteTeamLoadingLogo**](docs/TeamApi.md#deleteTeamLoadingLogo) | **DELETE** /teams/loading_logo | Delete team loading logo
*TeamApi* | [**deleteTeamLogo**](docs/TeamApi.md#deleteTeamLogo) | **DELETE** /teams/logo | Delete team logo
*TeamApi* | [**deleteTeamNadir**](docs/TeamApi.md#deleteTeamNadir) | **DELETE** /teams/nadir | Delete team nadir
*TeamApi* | [**getTeam**](docs/TeamApi.md#getTeam) | **GET** /teams | Get team
*TeamApi* | [**getTeamPreference**](docs/TeamApi.md#getTeamPreference) | **GET** /teams/preference | Team preference
*TeamApi* | [**removeUser**](docs/TeamApi.md#removeUser) | **DELETE** /teams/users/{uid} | Remove user
*TeamApi* | [**resendInvitation**](docs/TeamApi.md#resendInvitation) | **POST** /teams/resend_invitation | Resend invitation
*TeamApi* | [**undeleteTeam**](docs/TeamApi.md#undeleteTeam) | **PUT** /teams/undelete | Undelete team
*TeamApi* | [**updateTeam**](docs/TeamApi.md#updateTeam) | **PUT** /teams | Update team
*TeamApi* | [**updateTeamPreference**](docs/TeamApi.md#updateTeamPreference) | **PUT** /teams/preference | Update team preference
*TrashApi* | [**empty**](docs/TrashApi.md#empty) | **DELETE** /trashes/empty | Empty trash
*TrashApi* | [**getTrashes**](docs/TrashApi.md#getTrashes) | **GET** /trashes | Get trashed items
*TrashApi* | [**purge**](docs/TrashApi.md#purge) | **PUT** /trashes/purge | Purge trashed items
*TrashApi* | [**undelete**](docs/TrashApi.md#undelete) | **PUT** /trashes/undelete | Undelete trashed items
*UserApi* | [**acceptInvitation**](docs/UserApi.md#acceptInvitation) | **PUT** /users/accept_invitation | Accept team invitation
*UserApi* | [**confirmUserEmail**](docs/UserApi.md#confirmUserEmail) | **POST** /users/confirmation/{token} | Confirm user email
*UserApi* | [**getInvitation**](docs/UserApi.md#getInvitation) | **GET** /users/invitation | Get invitation info
*UserApi* | [**getUsers**](docs/UserApi.md#getUsers) | **GET** /users | Get users
*UserApi* | [**getUsersAutocomplete**](docs/UserApi.md#getUsersAutocomplete) | **GET** /users/autocomplete | Get users
*UserApi* | [**signup**](docs/UserApi.md#signup) | **POST** /users/signup | User Signup
*UserApi* | [**updateUser**](docs/UserApi.md#updateUser) | **PUT** /users/{uid} | Update user


## Documentation for Models

 - [Acl](docs/Acl.md)
 - [Asset](docs/Asset.md)
 - [AssetList](docs/AssetList.md)
 - [BaseTeam](docs/BaseTeam.md)
 - [BaseUser](docs/BaseUser.md)
 - [Comment](docs/Comment.md)
 - [CommentList](docs/CommentList.md)
 - [Comparison](docs/Comparison.md)
 - [ComparisonList](docs/ComparisonList.md)
 - [ErrorCode](docs/ErrorCode.md)
 - [Export](docs/Export.md)
 - [ExportList](docs/ExportList.md)
 - [FileEntity](docs/FileEntity.md)
 - [FileList](docs/FileList.md)
 - [HouseEntity](docs/HouseEntity.md)
 - [HouseEntityLocation](docs/HouseEntityLocation.md)
 - [HouseEntityShare](docs/HouseEntityShare.md)
 - [HouseList](docs/HouseList.md)
 - [Invoice](docs/Invoice.md)
 - [InvoiceList](docs/InvoiceList.md)
 - [Job](docs/Job.md)
 - [JobList](docs/JobList.md)
 - [LineItem](docs/LineItem.md)
 - [LineItemList](docs/LineItemList.md)
 - [Location](docs/Location.md)
 - [LodEndpointList](docs/LodEndpointList.md)
 - [Log](docs/Log.md)
 - [LogList](docs/LogList.md)
 - [Member](docs/Member.md)
 - [MemberList](docs/MemberList.md)
 - [Message](docs/Message.md)
 - [MessageList](docs/MessageList.md)
 - [OnboardList](docs/OnboardList.md)
 - [Pagination](docs/Pagination.md)
 - [PaymentMethod](docs/PaymentMethod.md)
 - [Plan](docs/Plan.md)
 - [PlanResponse](docs/PlanResponse.md)
 - [Product](docs/Product.md)
 - [ProductList](docs/ProductList.md)
 - [Project](docs/Project.md)
 - [ProjectList](docs/ProjectList.md)
 - [PurchaseOrder](docs/PurchaseOrder.md)
 - [PurchaseOrderList](docs/PurchaseOrderList.md)
 - [Redirect](docs/Redirect.md)
 - [SceneData](docs/SceneData.md)
 - [SearchResultItem](docs/SearchResultItem.md)
 - [SearchResults](docs/SearchResults.md)
 - [SectionEntity](docs/SectionEntity.md)
 - [SectionList](docs/SectionList.md)
 - [Session](docs/Session.md)
 - [StationEntity](docs/StationEntity.md)
 - [StationList](docs/StationList.md)
 - [StripeLineItem](docs/StripeLineItem.md)
 - [TagList](docs/TagList.md)
 - [TagListContents](docs/TagListContents.md)
 - [TeamProduct](docs/TeamProduct.md)
 - [TeamProductList](docs/TeamProductList.md)
 - [TeamStateInfo](docs/TeamStateInfo.md)
 - [TileEndpointList](docs/TileEndpointList.md)
 - [Trash](docs/Trash.md)
 - [TrashList](docs/TrashList.md)
 - [UserAutocompleteResponse](docs/UserAutocompleteResponse.md)
 - [UserList](docs/UserList.md)
 - [AccountManager](docs/AccountManager.md)
 - [Organization](docs/Organization.md)
 - [Team](docs/Team.md)
 - [User](docs/User.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### Bearer

- **Type**: API key
- **API key parameter name**: X-CUPIX-AUTH
- **Location**: HTTP header


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

inska.lee@cupix.com

