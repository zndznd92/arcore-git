package com.google.ar.core.examples.java.common.osc.request;

import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.ar.core.examples.java.common.osc.OSCVolley;
import com.google.ar.core.examples.java.common.osc.protocol.output.OSCResult;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCStatusRequest extends OSCRequest {
    private String OSCStatusURL = OSCURL + "/osc/commands/status";
    private boolean untilDone;
    public OSCStatusRequest(boolean untilDone){this.untilDone = untilDone;}

    @Override
    public JsonObjectRequest getRequest(final Context context, @Nullable final JSONObject param, final OSCRequestListener<JSONObject> listener, final OSCErrorListener errorListener, @Nullable final JsonObjectRequest afterReq) {
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, OSCStatusURL, param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                OSCResult result = new OSCResult(response, 2);
                Log.i("ar test", "command status(" + result.name + ") : " + result.state);

                if(untilDone){
                    if(!result.state.equals("done")){
                        OSCStatusRequest statusReq = new OSCStatusRequest(untilDone);
                        OSCVolley.getInstance(context).addToRequestQueue(statusReq.getRequest(context, param, listener, errorListener, afterReq));
                    }
                    else{
                        try {
                            Log.i("ar test", response.toString());
                            listener.onResponse(response);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        SystemClock.sleep(500);
                        if(afterReq != null) OSCVolley.getInstance(context).addToRequestQueue(afterReq);
                    }
                }
                else{
                    try {
                        listener.onResponse(response);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    if(afterReq != null) OSCVolley.getInstance(context).addToRequestQueue(afterReq);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("ar test", "json error", error);
            }
        });

        return request;
    }
}
