package com.google.ar.core.examples.java.common.osc.protocol.command;

import org.json.JSONObject;

public abstract class OSCCommand
{
    public String name;

    abstract public JSONObject getJSON(int API_VERSION);
}
