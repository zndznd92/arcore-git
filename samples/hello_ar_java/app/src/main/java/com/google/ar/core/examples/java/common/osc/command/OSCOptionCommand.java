package com.google.ar.core.examples.java.common.osc.command;

import com.google.ar.core.examples.java.common.osc.OSCCameraInfo;
import com.google.ar.core.examples.java.common.osc.OSCOption;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class OSCOptionCommand extends OSCCommand
{
    public OSCOptionCommand()
    {
        name = "camera.setOptions";
    }

    public String sessionId;
    public OSCOption options;
    public OSCCameraInfo info;

    @Override
    public JSONObject getJSON(int API_VERSION) {
        JSONObject obj = new JSONObject();

        try
        {
            JSONObject parameters = new JSONObject();
            if (API_VERSION == 1) {
                if(sessionId.length() == 0) return null;
                parameters.put("sessionId", sessionId);
            }

            //options
            JSONObject optionsJSON = new JSONObject();
            //clientVersion
            optionsJSON.put("clientVersion", options.clientLevel);

            parameters.put("options", optionsJSON);

            obj.put("name", name);
            obj.put("parameters", parameters);
        }
        catch(JSONException e)
        {
            Log.i("ar test", e.getMessage());
        }

        Log.i("ar test", obj.toString());

        return obj;
    }
}
