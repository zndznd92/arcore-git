package com.google.ar.core.examples.java.common.osc.request;
import android.content.Context;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

abstract public class OSCRequest {
    protected static String OSCURL = "http://192.168.1.1";

    public interface OSCRequestListener<T>{
        void onResponse(T response) throws JSONException;
    }

    public interface OSCErrorListener{
        void onErrorResponse(VolleyError error) throws JSONException;
    }

    abstract public JsonObjectRequest getRequest(final Context context, @Nullable JSONObject param, OSCRequestListener<JSONObject> listener, OSCErrorListener errorListener, @Nullable JsonObjectRequest afterReq);
}