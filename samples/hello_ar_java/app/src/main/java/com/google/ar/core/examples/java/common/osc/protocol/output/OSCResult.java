package com.google.ar.core.examples.java.common.osc.protocol.output;

import android.util.Log;

import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCCommandResult;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCStartSessionResult;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCTakePictureResult;

import org.json.JSONObject;

public class OSCResult
{
    public class OSCCommandError
    {
        public String code, message;
    }

    public class OSCCommandProgress
    {
        public String completion;
    }

    public String name;
    public String state;
    public String id;

    public OSCCommandError error;
    public OSCCommandProgress progress;
    public OSCCommandResult results;

    public boolean hasState = false;
    public boolean hasError = false;
    public boolean hasProgress = false;
    public boolean hasResults = false;

    public OSCResult(JSONObject obj, int API_VERSION)
    {
        name  = obj.optString("name");

        if(obj.has("state")) {
            hasState = true;
            state = obj.optString("state");
        }

        if(obj.has("id")) {
            id  = obj.optString("id");
        }

        if(obj.has("error")) {
            JSONObject errorObj = obj.optJSONObject("error");
            error = new OSCCommandError();
            error.code = errorObj.optString("code");
            error.message = errorObj.optString("message");
            hasError = true;
        }

        if(obj.has("progress")) {
            JSONObject progressObj = obj.optJSONObject("progress");
            progress = new OSCCommandProgress();
            progress.completion = progressObj.optString("completion");
            hasProgress = true;
        }

        if(obj.has("results")) {
            JSONObject resultsObj = obj.optJSONObject("results");
            switch (name) {
                case "camera.takePicture":
                    results = new OSCTakePictureResult();
                    break;
                case "camera.startSession":
                    results = new OSCStartSessionResult();
                    break;
                default :
                    results = null;
                    break;
            }

            results.processJSON(resultsObj, API_VERSION);
            hasResults = true;
        }
        else{
            Log.i("ar test", "no results");
        }
    }
}
