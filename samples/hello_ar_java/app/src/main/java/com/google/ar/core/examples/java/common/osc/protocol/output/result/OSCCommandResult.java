package com.google.ar.core.examples.java.common.osc.protocol.output.result;

import org.json.JSONObject;

abstract public class OSCCommandResult
{
    abstract public void processJSON(JSONObject obj, int API_VERSION);
}