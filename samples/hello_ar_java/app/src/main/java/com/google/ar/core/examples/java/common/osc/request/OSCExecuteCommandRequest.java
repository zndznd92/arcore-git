package com.google.ar.core.examples.java.common.osc.request;

abstract public class OSCExecuteCommandRequest extends OSCRequest {
    protected static String OSCExecuteCommandURL = OSCURL + "/osc/commands/execute";
}
