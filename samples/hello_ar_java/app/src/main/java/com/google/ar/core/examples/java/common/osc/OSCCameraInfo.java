package com.google.ar.core.examples.java.common.osc;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OSCCameraInfo
{
    public String manufacturer;
    public String model;
    public String serialNumber;
    public String firmwareVersion;
    public String supportUrl;

    public Boolean gps;
    public Boolean gyro;

    public int uptime;

    public String[] api;

    public OSCEndPoint endpoints;

    public int[] apiLevel;
    public String cameraId;

    public OSCCameraInfo(JSONObject obj){
        endpoints = new OSCEndPoint();
        try {
            this.processJSON(obj);
        }catch (JSONException e){
            Log.i("ar test", "unexpected JSON exception", e);
        }
    }

    public void processJSON(JSONObject obj) throws JSONException {
        this.manufacturer = obj.optString("manufacturer");
        this.model = obj.optString("model");
        this.serialNumber = obj.optString("serialNumber");
        this.firmwareVersion = obj.optString("firmwareVersion");
        this.supportUrl = obj.optString("supportUrl");

        this.gps = obj.optBoolean("gps");
        this.gyro = obj.optBoolean("gyro");

        this.uptime = obj.optInt("uptime");

        JSONArray apiArrays = obj.optJSONArray("api");

        this.api = new String[apiArrays.length()];
        for (int i = 0; i < apiArrays.length(); ++i) {
            this.api[i] = apiArrays.optString(i);
            Log.i("ar test", this.api[i]);
        }

        JSONObject endPoints = obj.getJSONObject("endpoints");
        this.endpoints.httpPort = endPoints.optInt("httpPort");
        this.endpoints.httpsPort = endPoints.optInt("httpsPort");
        this.endpoints.httpUpdatesPort= endPoints.optInt("httpUpdatesPort");
        this.endpoints.httpsUpdatesPort = endPoints.optInt("httpsUpdatesPort");

        if(obj.has("apiLevel"))
        {
            JSONArray apiLevelArrays = obj.optJSONArray("apiLevel");

            this.apiLevel = new int[apiLevelArrays.length()];
            for (int i = 0; i < apiLevelArrays.length(); ++i)
            {
                this.apiLevel[i] = apiLevelArrays.optInt(i);
            }
        }
        else
        {
            this.apiLevel = new int[1];
            this.apiLevel[0] = 1;
        }

        this.cameraId = obj.optString("cameraId");
    }
}
