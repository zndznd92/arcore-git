package com.google.ar.core.examples.java.common.osc.protocol.output.result;

import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class OSCTakePictureResult extends OSCCommandResult
{
    public String fileURL;

    @Override
    public void processJSON(JSONObject obj, int API_VERSION)
    {
        if(API_VERSION == 1)
            fileURL = obj.optString("fileUri");
        else if(API_VERSION == 2)
            fileURL = obj.optString("fileUrl");
    }
}