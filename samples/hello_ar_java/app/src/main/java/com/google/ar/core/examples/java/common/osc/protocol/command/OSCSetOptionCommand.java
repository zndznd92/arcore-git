package com.google.ar.core.examples.java.common.osc.protocol.command;

import com.google.ar.core.examples.java.common.osc.OSCCameraInfo;
import com.google.ar.core.examples.java.common.osc.OSCOption;
import android.util.Log;
import org.json.JSONException;
import org.json.JSONObject;

public class OSCSetOptionCommand extends OSCCommand
{
    public String sessionId;
    public OSCOption options;

    public OSCSetOptionCommand()
    {
        name = "camera.setOptions";
        sessionId = new String("");
    }

    @Override
    public JSONObject getJSON(int API_VERSION) {
        if(options == null) return null;

        JSONObject obj = new JSONObject();
        JSONObject parameters = new JSONObject();

        try
        {
            //if((this.sessionId != null) && (this.sessionId.length() != 0))
            if(API_VERSION == 1) {
                if(sessionId == null) return null;
                parameters.put("sessionId", this.sessionId);
            }

            //options
            JSONObject optionsJSON = new JSONObject();
            //clientVersion
            optionsJSON.put("clientVersion", this.options.clientLevel);

            //unite
            parameters.put("options", optionsJSON);
            obj.put("name", name);
            obj.put("parameters", parameters);
        }
        catch(JSONException e)
        {
            Log.i("ar test", e.getMessage());
        }

        Log.i("ar test", obj.toString());

        return obj;
    }
}
