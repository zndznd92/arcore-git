package com.google.ar.core.examples.java.common.osc.protocol.command;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.ar.core.examples.java.common.osc.protocol.output.OSCResult;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCTakePictureResult;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCStatusCommand
{
    private OSCStatusCommand() {
    }
    public OSCStatusCommand(String commandID, RequestQueue reqQueue){
        commandID_ = commandID;
        reqQueue_ = reqQueue;

        JSONObject param = new JSONObject();

        try {
            param.put("id", commandID_);
        }
        catch (JSONException e){
            Log.i("ar test", e.getMessage());
        }
    }

    private String URL = "/osc/commands/status";
    private JSONObject param;

    public String commandID_;
    public OSCCommand command_;
    public RequestQueue reqQueue_;

    private JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL, param, new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            OSCResult result = new OSCResult(response, 2);
            float completion = Float.parseFloat(result.progress.completion);

            if ((!result.hasState) || (result.state != "done")) {
                Log.i("ar test", "command status(" + result.name + ") : " + result.state);
                reqQueue_.add(request);
            }
        }
    }, new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            Log.i("ar test", error.getMessage());
        }
    });

    public JsonObjectRequest request()
    {
        return request;
    }
}
