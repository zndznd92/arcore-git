package com.google.ar.core.examples.java.common.osc;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.ar.core.examples.java.common.osc.protocol.command.OSCTakePictureCommand;
import com.google.ar.core.examples.java.common.osc.protocol.output.OSCResult;
import com.google.ar.core.examples.java.common.osc.protocol.command.OSCSetOptionCommand;
import com.google.ar.core.examples.java.common.osc.protocol.command.OSCStartSessionCommand;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCStartSessionResult;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCTakePictureResult;
import com.google.ar.core.examples.java.common.osc.request.OSCGetInfoRequest;
import com.google.ar.core.examples.java.common.osc.request.OSCRequest;
import com.google.ar.core.examples.java.common.osc.request.OSCRequest.OSCErrorListener;
import com.google.ar.core.examples.java.common.osc.request.OSCSetOptionRequest;
import com.google.ar.core.examples.java.common.osc.request.OSCStartSessionRequest;
import com.google.ar.core.examples.java.common.osc.request.OSCStatusRequest;
import com.google.ar.core.examples.java.common.osc.request.OSCTakePictureRequest;

import java.util.Map;
import java.util.Set;

public class OSCHelper {
    public String OSCURL = "http://192.168.1.1";
    public String OSCInfoURL = OSCURL + "/osc/info";
    public String OSCExecuteCommandURL = OSCURL + "/osc/commands/execute";
    public String OSCStateCommandURL = OSCURL + "/osc/commands/state";
    public String OSCStatusURL = OSCURL + "/osc/commands/status";

    private Context context;
    private String sessionId = "";

    private OSCCameraInfo info = null;
    private OSCOption options = null;

    private OSCHelper() {
        Initialize();
    }

    public OSCHelper(Context context) {
        this.context = context;
        options = new OSCOption();
        Initialize();
    }

    public int getAPIVersion(){
        return options.clientLevel;
    }

    public void Initialize(){
        getCameraInfo();
        setAPIVersion2();
    }

    public void getCameraInfo() {
        Log.i("Info", "Get Info!");
        OSCGetInfoRequest infoReq = new OSCGetInfoRequest();

        OSCVolley.getInstance(context).addToRequestQueue( infoReq.getRequest(context, null,
            new OSCRequest.OSCRequestListener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) throws JSONException {
                    info = new OSCCameraInfo(response);
                }
            },
            new OSCRequest.OSCErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) throws JSONException {
                    Log.i("ar test", "unexpected Volley Error", error);
                }
            }, null));
    }

    public void startSessions() {
        OSCStartSessionCommand command = new OSCStartSessionCommand();
        OSCStartSessionRequest startSessionReq = new OSCStartSessionRequest();

        OSCVolley.getInstance(context).addToRequestQueue(
            startSessionReq.getRequest(context, command.getJSON(1),
                new OSCRequest.OSCRequestListener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        OSCResult result = new OSCResult(response, 1);
                        OSCStartSessionResult startSessionResult = (OSCStartSessionResult)result.results;
                        sessionId = startSessionResult.sessionId;
                    }
                },
                new OSCRequest.OSCErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) throws JSONException {
                    }
                },
                null
            )
        );
    }

    public void setAPIVersion2() {
        OSCStartSessionCommand command = new OSCStartSessionCommand();
        OSCStartSessionRequest startSessionReq = new OSCStartSessionRequest();

        OSCVolley.getInstance(context).addToRequestQueue(
            startSessionReq.getRequest(context, command.getJSON(1),
                new OSCRequest.OSCRequestListener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                    OSCResult result = new OSCResult(response, 1);
                    OSCStartSessionResult startSessionResult = (OSCStartSessionResult)result.results;
                    sessionId = startSessionResult.sessionId;

                    Log.i("ar test", startSessionResult.toString());

                    OSCSetOptionRequest setOptionReq = new OSCSetOptionRequest();
                    OSCSetOptionCommand command = new OSCSetOptionCommand();
                    command.options = options;
                    command.options.clientLevel = 2;
                    command.sessionId = sessionId;

                    OSCVolley.getInstance(context).addToRequestQueue( setOptionReq.getRequest(context, command.getJSON(1), new OSCRequest.OSCRequestListener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) throws JSONException {
                            Log.i("ar test", "set apiversion 2 result : " + response.toString());
                            options.clientLevel = 2;
                        }
                    }, new OSCRequest.OSCErrorListener(){
                        @Override
                        public void onErrorResponse(VolleyError error) throws JSONException {
                            options.clientLevel = 1;
                            Log.i("ar test", "unexpected Volley Error", error);
                        }
                    }, null));
                    }
                },
                new OSCRequest.OSCErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) throws JSONException {
                        options.clientLevel = 2;
                    }
                },
                null
            )
        );
    }

    static boolean pictureComplete = true;
    public void takePicture(final OSCRequest.OSCRequestListener<JSONObject> listener)
    {
        /*
        if(!pictureComplete){
            return;
        }
        pictureComplete = false;
        //*/

        OSCTakePictureCommand command = new OSCTakePictureCommand(options.clientLevel, sessionId);
        final JSONObject params = command.getJSON(2);

        final OSCTakePictureRequest takePictureRequest = new OSCTakePictureRequest();
        OSCVolley.getInstance(context).addToRequestQueue(
            takePictureRequest.getRequest(context, params,
                new OSCRequest.OSCRequestListener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) throws JSONException {
                        OSCResult result = new OSCResult(response, options.clientLevel);
                        Log.i("ar test", "takePicture Result : " + response.toString());

                        if((result.state == null) || (!result.progress.equals("done")))
                        {
                            Log.i("ar test", "wait......");

                            JSONObject param = new JSONObject();
                            try {
                                param.put("id", result.id);
                            }
                            catch (JSONException e){
                                Log.i("ar test", e.getMessage());
                            }

                            OSCStatusRequest statusReq = new OSCStatusRequest(true);
                            OSCVolley.getInstance(context).addToRequestQueue(
                                statusReq.getRequest(context, param, listener, new OSCErrorListener(){
                                    @Override
                                    public void onErrorResponse(VolleyError error) throws JSONException {
                                        Log.i("ar test", "unexpected Volley Error : " + error.toString());
                                    }
                                }, null)
                            );
                        }
                        else
                        {
                            Log.i("ar test", "takepicture is done");
                        }
                    }
                },
                new OSCErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) throws JSONException {
                        Log.i("ar test", "unexpected Volley Error", error);
                    }
                }, null
            )
        );
    }
}
