package com.google.ar.core.examples.java.common.osc.command;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class OSCTakePictureCommand extends OSCCommand
{
    public String sessionId;

    public OSCTakePictureCommand()
    {
        name = "camera.takePicture";
    }

    @Override
    public JSONObject getJSON(int API_VERSION) {
        JSONObject obj = new JSONObject();

        try
        {
            JSONObject parameters = new JSONObject();
            if (API_VERSION == 1) {
                parameters.put("sessionId", sessionId);
            }

            obj.put("name", name);
            obj.put("parameters", parameters);
        }
        catch(JSONException e)
        {
            Log.i("ar test", e.getMessage());
        }

        Log.i("ar test", obj.toString());

        return obj;
    }
}
