package com.google.ar.core.examples.java.common.osc.protocol.command;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCGetOptionCommand extends OSCCommand
{
    OSCGetOptionCommand() {this.name = "camera.getOptions";}

    @Override
    public JSONObject getJSON(int API_VERSION) {
        JSONObject obj = new JSONObject();

        try {
            obj.put("name", this.name);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return obj;
    }
}
