package com.google.ar.core.examples.java.common.ar;

import android.content.Context;
import android.os.Environment;
import android.os.SystemClock;
import android.util.Log;

import com.google.ar.core.Frame;
import com.google.ar.core.Session;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ARPositionTracker
{
    private static ARPositionTracker instance;
    public static String fileName = "position.txt";
    private static boolean stopTracking;
    private static Session session;
    public static Frame frame;

    public static ARPositionTracker getInstance() {
        if(instance == null) {
            instance = new ARPositionTracker();
        }

        return instance;
    }

    public static void stopTracking(){
        stopTracking = true;
    }

    public Thread startTracker(final Context context, final long periodMs)
    {
        Log.i("ar test", "start Tracking");
        return new Thread(){
            public void run(){
            File file = new File(context.getExternalFilesDir(null), fileName);

            if(!file.exists()){
                Log.i("ar test", file.getAbsolutePath() + "isn't exist!");
            }
            if(!file.canWrite()){
                Log.i("ar test", file.getAbsolutePath() + "can't write!");
            }
            //*
            FileOutputStream stream;
            try {
                //stream = context.openFileOutput(file);
                stream = new FileOutputStream(file);
                Log.i("ar test", file.getAbsolutePath());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return;
            }
            //*/

            while(!stopTracking){
                if(frame == null) continue;

                String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS", Locale.getDefault()).format(new Date());
                final float[] pos = frame.getCamera().getPose().getTranslation();

                String record = String.format(Locale.getDefault(),"%s : %f %f %f\n", date, pos[0], pos[1], pos[2]);
                Log.i("ar test", "record position : " + record);

                //*
                try {
                    stream.write(record.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //*/

                SystemClock.sleep(periodMs);
            }

            try {
                stream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            }
        };
    }
}
