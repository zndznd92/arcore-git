/*
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.google.ar.core.examples.java.helloar;

import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.ar.core.Anchor;
import com.google.ar.core.ArCoreApk;
import com.google.ar.core.Camera;
import com.google.ar.core.Frame;
import com.google.ar.core.HitResult;
import com.google.ar.core.Plane;
import com.google.ar.core.Point;
import com.google.ar.core.Point.OrientationMode;
import com.google.ar.core.PointCloud;
import com.google.ar.core.Session;
import com.google.ar.core.Trackable;
import com.google.ar.core.TrackingState;
import com.google.ar.core.examples.java.common.ar.ARPositionTracker;
import com.google.ar.core.examples.java.common.helpers.CameraPermissionHelper;
import com.google.ar.core.examples.java.common.helpers.DisplayRotationHelper;
import com.google.ar.core.examples.java.common.helpers.FullScreenHelper;
import com.google.ar.core.examples.java.common.helpers.SnackbarHelper;
import com.google.ar.core.examples.java.common.helpers.TapHelper;
import com.google.ar.core.examples.java.common.osc.OSCHelper;
import com.google.ar.core.examples.java.common.osc.OSCVolley;
import com.google.ar.core.examples.java.common.osc.protocol.output.OSCResult;
import com.google.ar.core.examples.java.common.osc.protocol.output.result.OSCTakePictureResult;
import com.google.ar.core.examples.java.common.osc.request.OSCRequest;
import com.google.ar.core.examples.java.common.rendering.BackgroundRenderer;
import com.google.ar.core.examples.java.common.rendering.ObjectRenderer;
import com.google.ar.core.examples.java.common.rendering.ObjectRenderer.BlendMode;
import com.google.ar.core.examples.java.common.rendering.PlaneRenderer;
import com.google.ar.core.examples.java.common.rendering.PointCloudRenderer;
import com.google.ar.core.exceptions.CameraNotAvailableException;
import com.google.ar.core.exceptions.UnavailableApkTooOldException;
import com.google.ar.core.exceptions.UnavailableArcoreNotInstalledException;
import com.google.ar.core.exceptions.UnavailableDeviceNotCompatibleException;
import com.google.ar.core.exceptions.UnavailableSdkTooOldException;
import com.google.ar.core.exceptions.UnavailableUserDeclinedInstallationException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import io.swagger.client.ApiException;
import io.swagger.client.ApiInvoker;
import io.swagger.client.api.AuthenticateApi;
import io.swagger.client.api.FileApi;
import io.swagger.client.api.HouseApi;
import io.swagger.client.api.ProjectApi;
import io.swagger.client.api.SectionApi;
import io.swagger.client.model.ErrorCode;
import io.swagger.client.model.FileEntity;
import io.swagger.client.model.HouseEntity;
import io.swagger.client.model.Project;
import io.swagger.client.model.SectionEntity;
import io.swagger.client.model.User;

/**
 * This is a simple example that shows how to create an augmented reality (AR) application using the
 * ARCore API. The application will display any detected planes and will allow the user to tap on a
 * plane to place a 3d model of the Android robot.
 */
public class HelloArActivity extends AppCompatActivity implements GLSurfaceView.Renderer {
  private static final String TAG = HelloArActivity.class.getSimpleName();

  // Rendering. The Renderers are created here, and initialized when the GL surface is created.
  private GLSurfaceView surfaceView;

  private boolean installRequested;

  private Session session;
  private final SnackbarHelper messageSnackbarHelper = new SnackbarHelper();
  private DisplayRotationHelper displayRotationHelper;
  private TapHelper tapHelper;

  private final BackgroundRenderer backgroundRenderer = new BackgroundRenderer();
  private final ObjectRenderer virtualObject = new ObjectRenderer();
  private final ObjectRenderer virtualObjectShadow = new ObjectRenderer();
  private final PlaneRenderer planeRenderer = new PlaneRenderer();
  private final PointCloudRenderer pointCloudRenderer = new PointCloudRenderer();

  // Temporary matrix allocated here to reduce number of allocations for each frame.
  private final float[] anchorMatrix = new float[16];

  // Anchors created from taps used for object placing.
  private final ArrayList<Anchor> anchors = new ArrayList<>();

  //for open spherical camera api
  private Thread photoThread;
  private OSCHelper helper;

  //authenticate
  private String email = "rnd.cloud@cupix.com";
  private String pwd = "PleaseUpdateAsana!";

  private ArrayList<String> imageURLS = new ArrayList<String>();
  private ArrayList<Bitmap> bitmaps = new ArrayList<Bitmap>();
  BitmapRecorder recorder = new BitmapRecorder();

  private boolean stopAR = false;

  private String getStringImage(Bitmap bmp){
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
      byte[] imageBytes = baos.toByteArray();
      String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
      return encodedImage;
  }

  private Bitmap loadBitmap(String fileName)
  {
      File root =  this.getExternalFilesDir(null);
      return BitmapFactory.decodeFile(root+ "/" + fileName);
  }

  protected void onCreate(Bundle savedInstanceState) {
      recorder.setContext(this);

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    surfaceView = findViewById(R.id.surfaceview);
    displayRotationHelper = new DisplayRotationHelper(/*context=*/ this);

    // Set up tap listener.
    tapHelper = new TapHelper(/*context=*/ this);
    surfaceView.setOnTouchListener(tapHelper);

    // Set up renderer.
    surfaceView.setPreserveEGLContextOnPause(true);
    surfaceView.setEGLContextClientVersion(2);
    surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
    surfaceView.setRenderer(this);
    surfaceView.setRenderMode(GLSurfaceView.RENDERMODE_CONTINUOUSLY);

    installRequested = false;

    //set up click listener
    final Context context = this;
    helper = new OSCHelper(context);

    final Button button = findViewById(R.id.GetInfo);
    button.setOnClickListener(new View.OnClickListener(){
      public void onClick(View v){
        Thread apiThread = new Thread(new Runnable() {
          @Override
          public void run() {
            helper.getCameraInfo();
          }
        });
        apiThread.start();
      }
    });

    final Button takePictureButton = findViewById(R.id.TakePicture);
    takePictureButton.setOnClickListener(new View.OnClickListener(){
        public void onClick(View v){
            photoThread = new Thread() {
                public void run() {
                    helper.takePicture(new OSCRequest.OSCRequestListener<JSONObject>(){
                        @Override
                        public void onResponse(JSONObject response) throws JSONException {
                            OSCResult result = new OSCResult(response, helper.getAPIVersion());
                            OSCTakePictureResult oscResult =(OSCTakePictureResult)result.results;
                            Log.i("ar test", "fileURL : " + oscResult.fileURL);
                            imageURLS.add(oscResult.fileURL);

                            ImageLoader imageLoader = OSCVolley.getInstance(context).getImageLoader();
                            imageLoader.get(oscResult.fileURL, new ImageLoader.ImageListener() {
                                @Override
                                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                                    Bitmap resultBitmap = response.getBitmap();
                                    if(resultBitmap == null){
                                        Log.i("ar test", "Fetching image : " + response.getRequestUrl());
                                        return;
                                    }

                                    bitmaps.add(resultBitmap);
                                    Log.i("ar test", "Fetching image complete !");
                                    ImageView imageView  = findViewById(R.id.imageView);
                                    imageView.setImageBitmap(resultBitmap);
                                    recorder.saveNewBitmap(resultBitmap);
                                }

                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.i("ar test", "failed to get image : " + error.getMessage());
                                }
                            });
                        }
                    });
                }
            };
            photoThread.start();
        }
    });

    final Button startCaptureButton = findViewById(R.id.StartCapture);
      startCaptureButton.setOnClickListener(new View.OnClickListener(){
      public void onClick(View v){
        Log.i("ar test", "start position track thread");
        Thread positionTrackingThread = ARPositionTracker.getInstance().startTracker(context, 500);
        positionTrackingThread.start();
      }
    });

    final Button stopPictureButton = findViewById(R.id.StopCapture);
    stopPictureButton.setOnClickListener(new View.OnClickListener(){
      public void onClick(View v){
        ARPositionTracker.stopTracking();
      }
    });

    final Button uploadButton = findViewById(R.id.Upload);
    uploadButton.setOnClickListener(new View.OnClickListener(){
        @Override
        public void onClick(View v) {
        Thread authThread = new Thread(new Runnable() {
            @Override
            public void run() {
            Project project = null;
            ProjectApi projectApi = new ProjectApi();
            try {
                //authenticate user
                AuthenticateApi auth = new AuthenticateApi();
                User user = null;
                user = auth.authenticate("rnd.cloud@cupix.com", "PleaseUpdateAsana!", "firstname, lastname, email, access_token");

                ApiInvoker invoker = ApiInvoker.getInstance();
                invoker.setApiKey(user.getAccessToken());
                invoker.setApiKeyPrefix("Bearer");

                if(user == null && user.getAccessToken() == null ){
                    return;
                }

                //create project
                Integer projectId = 11818;
                /*
                project = projectApi.createProject("android test", "Pangyo-ro, Bundang-gu, Seongnam-si, Gyeonggi-do, South Korea", "{\"latitude\":37.3985507,\"longitude\":127.1068146}", "", "id");
                Log.i("ar test", "new project : " + project.getId());
                if(project == null) return;
                //*/

                //create house
                HouseApi houseApi = new HouseApi();
                HouseEntity houseEntity = houseApi.createHouse(projectId, "new house", "", "", "", "key");
                Log.i("ar test", "new house : " + houseEntity.getKey());

                //create section
                SectionApi sectionApi = new SectionApi();
                SectionEntity sectionEntity = sectionApi.createSection(houseEntity.getKey(), "android test", "", "", "key");
                Log.i("ar test", "new section : " + houseEntity.getKey());

                //upload files
                String S3Url;
                final FileApi fileApi = new FileApi();
                //Iterator<String> fileNameIterator = recorder.getFileNameIterator();
                Iterator<Bitmap> imageIterator = bitmaps.iterator();
                int i = -1;
                while(imageIterator.hasNext())
                {
                    ++i;
                    //String fileName = fileNameIterator.next();
                    final FileEntity fileEntity = fileApi.createFile(i + ".jpg", sectionEntity.getKey(), projectId, "spherical_image", "all");
                    Log.i("ar test", fileEntity.toString());
                    //Bitmap sphereImg = loadBitmap(fileName);
                    //final String imageString = getStringImage(sphereImg);
                    final String imageString = getStringImage(imageIterator.next());

                    stopAR = true;
                    //PUT REQUEST
                    StringRequest putRequest = new StringRequest(Request.Method.PUT, fileEntity.getAction(),
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                stopAR = false;
                                // response
                                Log.i("ar test",  "File Success : " + response);
                                //finalize file
                                try {
                                    ErrorCode code = fileApi.finalizeFileUpload(fileEntity.getKey(), "all");
                                    Log.i("ar test", "Finalize Result : " + code.getReason());
                                } catch (TimeoutException e) {
                                    e.printStackTrace();
                                } catch (ExecutionException e) {
                                    e.printStackTrace();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                } catch (ApiException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                stopAR = false;
                                // error
                                Log.i("ar test",  "File Error : " + error.toString());
                            }
                        }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("Cache-Control", "max-age=31536000");
                            params.put("Content-Type", "image/jpeg");

                            return params;
                        }
                        @Override
                        {
                            Map<String, String>  params = new HashMap<String, String>();
                            params.put("data", imageString);
                            return params;
                        }
                    };

                    OSCVolley.getInstance(context).addToRequestQueue(putRequest);
                }

            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ApiException e) {
                e.printStackTrace();
            }
            }
        });
        authThread.start();
        }
    });
  }

  @Override
  protected void onResume() {
    super.onResume();

    if (session == null) {
      Exception exception = null;
      String message = null;
      try {
        switch (ArCoreApk.getInstance().requestInstall(this, !installRequested)) {
          case INSTALL_REQUESTED:
            installRequested = true;
            return;
          case INSTALLED:
            break;
        }

        // ARCore requires camera permissions to operate. If we did not yet obtain runtime
        // permission on Android M and above, now is a good time to ask the user for it.
        if (!CameraPermissionHelper.hasCameraPermission(this)) {
          CameraPermissionHelper.requestCameraPermission(this);
          return;
        }

        // Create the session.
        session = new Session(this);

      } catch (UnavailableArcoreNotInstalledException
          | UnavailableUserDeclinedInstallationException e) {
        message = "Please install ARCore";
        exception = e;
      } catch (UnavailableApkTooOldException e) {
        message = "Please update ARCore";
        exception = e;
      } catch (UnavailableSdkTooOldException e) {
        message = "Please update this app";
        exception = e;
      } catch (UnavailableDeviceNotCompatibleException e) {
        message = "This device does not support AR";
        exception = e;
      } catch (Exception e) {
        message = "Failed to create AR session";
        exception = e;
      }

      if (message != null) {
        messageSnackbarHelper.showError(this, message);
        Log.e(TAG, "Exception creating session", exception);
        return;
      }
    }

    // Note that order matters - see the note in onPause(), the reverse applies here.
    try {
      session.resume();
    } catch (CameraNotAvailableException e) {
      // In some cases (such as another camera app launching) the camera may be given to
      // a different app instead. Handle this properly by showing a message and recreate the
      // session at the next iteration.
      messageSnackbarHelper.showError(this, "Camera not available. Please restart the app.");
      session = null;
      return;
    }

    surfaceView.onResume();
    displayRotationHelper.onResume();

    messageSnackbarHelper.showMessage(this, "Searching for surfaces...");
  }

  @Override
  public void onPause() {
    super.onPause();
    if (session != null) {
      // Note that the order matters - GLSurfaceView is paused first so that it does not try
      // to query the session. If Session is paused before GLSurfaceView, GLSurfaceView may
      // still call session.update() and get a SessionPausedException.
      displayRotationHelper.onPause();
      surfaceView.onPause();
      session.pause();
    }
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] results) {
    if (!CameraPermissionHelper.hasCameraPermission(this)) {
      Toast.makeText(this, "Camera permission is needed to run this application", Toast.LENGTH_LONG)
          .show();
      if (!CameraPermissionHelper.shouldShowRequestPermissionRationale(this)) {
        // Permission denied with checking "Do not ask again".
        CameraPermissionHelper.launchPermissionSettings(this);
      }
      finish();
    }
  }

  @Override
  public void onWindowFocusChanged(boolean hasFocus) {
    super.onWindowFocusChanged(hasFocus);
    FullScreenHelper.setFullScreenOnWindowFocusChanged(this, hasFocus);
  }

  @Override
  public void onSurfaceCreated(GL10 gl, EGLConfig config) {
    GLES20.glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    // Prepare the rendering objects. This involves reading shaders, so may throw an IOException.
    try {
      // Create the texture and pass it to ARCore session to be filled during update().
      backgroundRenderer.createOnGlThread(/*context=*/ this);
      planeRenderer.createOnGlThread(/*context=*/ this, "models/trigrid.png");
      pointCloudRenderer.createOnGlThread(/*context=*/ this);

      virtualObject.createOnGlThread(/*context=*/ this, "models/andy.obj", "models/andy.png");
      virtualObject.setMaterialProperties(0.0f, 2.0f, 0.5f, 6.0f);

      virtualObjectShadow.createOnGlThread(
          /*context=*/ this, "models/andy_shadow.obj", "models/andy_shadow.png");
      virtualObjectShadow.setBlendMode(BlendMode.Shadow);
      virtualObjectShadow.setMaterialProperties(1.0f, 0.0f, 0.0f, 1.0f);

    } catch (IOException e) {
      Log.e(TAG, "Failed to read an asset file", e);
    }
  }

  @Override
  public void onSurfaceChanged(GL10 gl, int width, int height) {
    displayRotationHelper.onSurfaceChanged(width, height);
    GLES20.glViewport(0, 0, width, height);
  }

  @Override
  public void onDrawFrame(GL10 gl) {
//*
    // Clear screen to notify driver it should not load any pixels from previous frame.
    GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

    if (session == null) {
      return;
    }
    // Notify ARCore session that the view size changed so that the perspective matrix and
    // the video background can be properly adjusted.
    displayRotationHelper.updateSessionIfNeeded(session);

    try {
        if(stopAR)
            return;
      session.setCameraTextureName(backgroundRenderer.getTextureId());

      // Obtain the current frame from ARSession. When the configuration is set to
      // UpdateMode.BLOCKING (it is by default), this will throttle the rendering to the
      // camera framerate.
      Frame frame = session.update();
      ARPositionTracker.frame = frame;
      Camera camera = frame.getCamera();

      // Handle taps. Handling only one tap per frame, as taps are usually low frequency
      // compared to frame rate.
      MotionEvent tap = tapHelper.poll();
      if (tap != null && camera.getTrackingState() == TrackingState.TRACKING) {
        for (HitResult hit : frame.hitTest(tap)) {
          // Check if any plane was hit, and if it was hit inside the plane polygon
          Trackable trackable = hit.getTrackable();
          // Creates an anchor if a plane or an oriented point was hit.
          if ((trackable instanceof Plane
                  && ((Plane) trackable).isPoseInPolygon(hit.getHitPose())
                  && (PlaneRenderer.calculateDistanceToPlane(hit.getHitPose(), camera.getPose())
                      > 0))
              || (trackable instanceof Point
                  && ((Point) trackable).getOrientationMode()
                      == OrientationMode.ESTIMATED_SURFACE_NORMAL)) {
            // Hits are sorted by depth. Consider only closest hit on a plane or oriented point.
            // Cap the number of objects created. This avoids overloading both the
            // rendering system and ARCore.
            if (anchors.size() >= 20) {
              anchors.get(0).detach();
              anchors.remove(0);
            }
            // Adding an Anchor tells ARCore that it should track this position in
            // space. This anchor is created on the Plane to place the 3D model
            // in the correct position relative both to the world and to the plane.
            anchors.add(hit.createAnchor());
            break;
          }
        }
      }

      // Draw background.
      backgroundRenderer.draw(frame);

      // If not tracking, don't draw 3d objects.
      if (camera.getTrackingState() == TrackingState.PAUSED) {
        return;
      }

      // Get projection matrix.
      float[] projmtx = new float[16];
      camera.getProjectionMatrix(projmtx, 0, 0.1f, 100.0f);

      // Get camera matrix and draw.
      float[] viewmtx = new float[16];
      camera.getViewMatrix(viewmtx, 0);

      // Compute lighting from average intensity of the image.
      // The first three components are color scaling factors.
      // The last one is the average pixel intensity in gamma space.
      final float[] colorCorrectionRgba = new float[4];
      frame.getLightEstimate().getColorCorrection(colorCorrectionRgba, 0);

      // Visualize tracked points.
      PointCloud pointCloud = frame.acquirePointCloud();
      pointCloudRenderer.update(pointCloud);
      pointCloudRenderer.draw(viewmtx, projmtx);

      // Application is responsible for releasing the point cloud resources after
      // using it.
      pointCloud.release();

      // Check if we detected at least one plane. If so, hide the loading message.
      if (messageSnackbarHelper.isShowing()) {
        for (Plane plane : session.getAllTrackables(Plane.class)) {
          if (plane.getType() == com.google.ar.core.Plane.Type.HORIZONTAL_UPWARD_FACING
              && plane.getTrackingState() == TrackingState.TRACKING) {
            messageSnackbarHelper.hide(this);
            break;
          }
        }
      }

      // Visualize planes.
      planeRenderer.drawPlanes(
          session.getAllTrackables(Plane.class), camera.getDisplayOrientedPose(), projmtx);

      // Visualize anchors created by touch.
      float scaleFactor = 1.0f;
      for (Anchor anchor : anchors) {
        if (anchor.getTrackingState() != TrackingState.TRACKING) {
          continue;
        }
        // Get the current pose of an Anchor in world space. The Anchor pose is updated
        // during calls to session.update() as ARCore refines its estimate of the world.
        anchor.getPose().toMatrix(anchorMatrix, 0);

        // Update and draw the model and its shadow.
        virtualObject.updateModelMatrix(anchorMatrix, scaleFactor);
        virtualObjectShadow.updateModelMatrix(anchorMatrix, scaleFactor);
        virtualObject.draw(viewmtx, projmtx, colorCorrectionRgba);
        virtualObjectShadow.draw(viewmtx, projmtx, colorCorrectionRgba);
      }

    } catch (Throwable t) {
      // Avoid crashing the application due to unhandled exceptions.
      Log.e(TAG, "Exception on the OpenGL thread", t);
    }
    //*/
  }
}
