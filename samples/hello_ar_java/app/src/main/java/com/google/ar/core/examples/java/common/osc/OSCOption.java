package com.google.ar.core.examples.java.common.osc;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCOption
{
    public int clientLevel;

    public String getCaptureMode(){
        String result = new String();

        try {
            result = obj.getString("captureMode");
        }catch(JSONException e){
            Log.i("ar test", e.getMessage());
        }

        return result;
    }
    public String[] captureModeSupport;

    public String captureStatus;
    public String[] captureStatusSupport;

    public int exposureProgram;
    public int[] exposureProgramSupport;

    public int iso;
    public int[] isoSupport;

    public int shutterSpeed;
    public int[] shutterSpeedSupport;

    public int aperture;
    public int[] apertureSupport;

    public String whiteBalance;
    public String whiteBalanceSupport;

    private JSONObject obj;
}
