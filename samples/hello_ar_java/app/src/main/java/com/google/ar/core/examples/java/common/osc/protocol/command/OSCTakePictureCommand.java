package com.google.ar.core.examples.java.common.osc.protocol.command;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCTakePictureCommand extends OSCCommand
{
    public String sessionId;

    public OSCTakePictureCommand(int APIVersion, String sessionId)
    {
        name = "camera.takePicture";
        if(APIVersion == 1) {
            if((sessionId == null) || (sessionId.length() < 1)){
                return;
            }
            sessionId = sessionId;
        }
    }

    @Override
    public JSONObject getJSON(int API_VERSION) {
        JSONObject obj = new JSONObject();

        try
        {
            JSONObject parameters = new JSONObject();
            if (API_VERSION == 1) {
                parameters.put("sessionId", sessionId);
            }
            obj.put("name", name);
        }
        catch(JSONException e)
        {
            Log.i("ar test", e.getMessage());
        }

        Log.i("ar test", obj.toString());

        return obj;
    }
}
