package com.google.ar.core.examples.java.common.osc.request;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.ar.core.examples.java.common.osc.OSCVolley;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCTakePictureRequest extends OSCExecuteCommandRequest {
    @Override
    public JsonObjectRequest getRequest(final Context context, @Nullable JSONObject param, final OSCRequestListener<JSONObject> listener, final OSCErrorListener errorListener, @Nullable final JsonObjectRequest afterReq) {
        JsonObjectRequest pictureReq = new JsonObjectRequest (Request.Method.POST, OSCExecuteCommandURL, param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    listener.onResponse(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (afterReq != null) {
                    Log.i("ar test", "add additional request");
                    OSCVolley.getInstance(context).addToRequestQueue(afterReq);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    errorListener.onErrorResponse(error);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.i("ar test", "unexpected Volley Error", error);
            }
        });

        return pictureReq;
    }
}
