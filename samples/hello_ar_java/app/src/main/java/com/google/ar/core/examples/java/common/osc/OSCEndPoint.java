package com.google.ar.core.examples.java.common.osc;

public class OSCEndPoint {
    public int httpPort;
    public int httpUpdatesPort;
    public int httpsPort;
    public int httpsUpdatesPort;
}
