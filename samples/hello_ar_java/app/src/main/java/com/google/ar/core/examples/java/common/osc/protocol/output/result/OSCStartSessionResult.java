package com.google.ar.core.examples.java.common.osc.protocol.output.result;

import android.util.Log;
import org.json.JSONObject;

public class OSCStartSessionResult extends  OSCCommandResult
{
    public String sessionId;
    public int timeOut;

    @Override
    public void processJSON(JSONObject obj, int API_VERSION)
    {
        if(obj.has("sessionId")) sessionId = obj.optString("sessionId");
        if(obj.has("timeOut")) timeOut = obj.optInt("timeOut");

        Log.i("ar test", "sessionId : " + sessionId);
    }
}
