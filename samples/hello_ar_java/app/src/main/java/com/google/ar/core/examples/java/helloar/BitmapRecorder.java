package com.google.ar.core.examples.java.helloar;

import android.content.Context;
import android.graphics.Bitmap;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

public class BitmapRecorder
{
    private ArrayList<String> fileNames = new ArrayList<String>();
    private Context context;

    public void setContext(Context context)
    {
        this.context = context;
    }

    public void saveNewBitmap(Bitmap bitmap)
    {
        String fileName = fileNames.size() + ".jpg";
        File file = new File(context.getExternalFilesDir(null), fileName);
        try {
            OutputStream os = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            os.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileNames.add(fileName);
    }

    public Iterator<String> getFileNameIterator(){
        return fileNames.iterator();
    }
}
