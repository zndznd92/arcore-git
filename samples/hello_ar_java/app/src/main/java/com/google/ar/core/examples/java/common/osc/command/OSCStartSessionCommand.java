package com.google.ar.core.examples.java.common.osc.command;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCStartSessionCommand extends OSCCommand {

    public OSCStartSessionCommand()
    {
        name = "camera.startSession";
    }

    @Override
    public JSONObject getJSON(int API_VERSION) {
        JSONObject obj = new JSONObject();

        try
        {
            obj.put("name", name);
        }
        catch(JSONException e)
        {
            Log.i("ar test", e.getMessage());
        }

        Log.i("ar test", obj.toString());

        return obj;
    }
}
