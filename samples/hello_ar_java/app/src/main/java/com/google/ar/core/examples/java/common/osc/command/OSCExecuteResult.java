package com.google.ar.core.examples.java.common.osc.command;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class OSCExecuteResult
{
    abstract public class OSCCommandResult
    {
        abstract public void processJSON(JSONObject obj, int API_VERSION);
    }

    public class OSCTakePictureResult extends OSCCommandResult
    {
        public String fileURL;

        @Override
        public void processJSON(JSONObject obj, int API_VERSION)
        {
            try {
                JSONObject resultsObj = obj.getJSONObject("results");

                if(API_VERSION == 1)
                    fileURL = resultsObj.optString("fileUri");
                else if(API_VERSION == 2)
                    fileURL = resultsObj.optString("fileUrl");
            }
            catch(JSONException e){
                Log.i("Unexpected exception (takePicture command)", e.getMessage());
            }
        }
    }

    public class OSCStartSessionResult extends  OSCCommandResult
    {
        public String sessionId;
        public int timeOut;

        @Override
        public void processJSON(JSONObject obj, int API_VERSION)
        {
            if(obj.has("sessionId")) sessionId = obj.optString("sessionId");
            if(obj.has("timeOut")) timeOut = obj.optInt("timeOut");

            Log.i("ar test", "sessionId : " + sessionId);
        }
    }

    public class OSCCommandError
    {
        public String code, message;
    }

    public class OSCCommandProgress
    {
        public String completion;
    }

    public String name;
    public String state;
    public String id;

    public OSCCommandError error;
    public OSCCommandProgress progress;
    public OSCCommandResult results;

    public OSCExecuteResult(JSONObject obj, int API_VERSION)
    {
        name  = obj.optString("name");
        state  = obj.optString("state");

        Log.i("ar test", "execute result(" + name + ") : " + state);

        if(obj.has("id")) id  = obj.optString("id");

        if(obj.has("error")) {
            JSONObject errorObj = obj.optJSONObject("error");
            error = new OSCCommandError();
            error.code = errorObj.optString("code");
            error.message = errorObj.optString("message");
        }

        if(obj.has("progress")) {
            JSONObject progressObj = obj.optJSONObject("progress");
            Log.i("ar test", "Result(progress)" + progressObj.toString());
            progress = new OSCCommandProgress();
            progress.completion = progressObj.optString("completion");
        }

        if(obj.has("results")) {
            JSONObject resultsObj = obj.optJSONObject("results");
            Log.i("ar test", "Result(results)" + resultsObj.toString());

            switch (name) {
                case "camera.takePicture":
                    results = new OSCTakePictureResult();
                case "camera.startSession":
                    results = new OSCStartSessionResult();
                    break;
            }

            results.processJSON(resultsObj, API_VERSION);
        }
    }
}
